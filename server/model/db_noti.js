/**
 * Created by Shin Yeonsik on 2015-05-19.
 */

var db = require('../model/db');
var wlog = require('../common/wlogger');
var s = require('../control/sessionHelper');

/*
callback signature: cb(err, notiId)
 */
exports.addNoti = function(type, userId, info, cb){
	var logPart = 'model.db_noti.addNoti';
	var logHead = s.getSessionId()+' ['+logPart+']';
	var infoString = JSON.stringify(info);
	db.dbDataManipulate(
		'INSERT INTO noti (noti_id, user_id, reg_tm, checked, type, info) ' +
		'VALUES(null, ?, now(), false, ?, ?)',
		[userId, type, infoString],
		logPart,
		function(err, success, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : db query');
				return cb(err, null);
			}
			if( success ){
				return cb(err, result.insertId);
			}else{
				err = new Error('query failed');
				wlog.error({err:err}, logHead+' ERROR');
				return cb(err, null);
			}
		}
	);
}

/*
callback signature: cb(err, notiList)
 */
exports.getNotiList = function(userId, cb){
	var logPart = 'model.db_noti.getNotiList';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT * FROM noti WHERE user_id=? AND (checked=0 OR TIMEDIFF(NOW(), reg_tm) <= 31)',
		[userId],
		logPart,
		function(err, notiList){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : sql fail');
			}
			return cb(err, notiList);
		}
	);
}

/*
callback signature: cb(err)
 */
exports.setNotiRead = function(notiId, cb){
	var logPart = 'model.db_noti.setNotiRead';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'UPDATE noti SET checked=1 WHERE noti_id=?',
		[notiId],
		logPart,
		function(err, success, result){
			if(!err && false === success){
				err = new Error('query fail');
			}
			if(err){
				wlog.error({err:err}, logHead);
			}
			return cb(err);
		}
	);
}