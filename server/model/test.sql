SELECT * FROM messages
WHERE (sender_id=1 AND receiver_id=8) OR (sender_id=8 AND receiver_id=1);

SELECT * FROM messages WHERE sender_id=3 OR receiver_id=3


INSERT INTO users
    (user_id, email, nickname, acc_conn_type, type, reg_tm,
        birthday, gender, pic_folder, pic_file, password, salt)
    VALUES(
        null, 'choninchonin@naver.com', 'chonin', 0, 1,
        now(), '1990-10-10', 'M', '/images', 'profile_ex.jpg',
        '5ca9c9f6aae284c36a061a9af978eaf40be47a05fb77f6dbf0720538387cce82c61feaa771206600cc7f2aa6bd61b595c08b2fa91b67a60f5d4b1e32f3ce34b6',
        'xrA4E5dVF1jxUHPORbFntxX7uGkla898KG3NfFRxQcI479x94E'
    );

INSERT INTO users
    (user_id, email, nickname, acc_conn_type, type, reg_tm,
        birthday, gender, pic_folder, pic_file, password, salt)
    VALUES(
        null, 'happyit@happynamic.com', 'happyit84', 0, 1,
        now(), '1990-03-10', 'F', '/images', 'profile_ex.jpg',
        '5ca9c9f6aae284c36a061a9af978eaf40be47a05fb77f6dbf0720538387cce82c61feaa771206600cc7f2aa6bd61b595c08b2fa91b67a60f5d4b1e32f3ce34b6',
        'xrA4E5dVF1jxUHPORbFntxX7uGkla898KG3NfFRxQcI479x94E'
    );

INSERT INTO users
    (user_id, email, nickname, acc_conn_type, type, reg_tm,
        birthday, gender, pic_folder, pic_file, password, salt)
    VALUES(
        null, 'choninchonin@gmail.com', 'choninchonin', 0, 1,
        now(), '1970-01-15', 'F', '/images', 'profile_ex.jpg',
        '5ca9c9f6aae284c36a061a9af978eaf40be47a05fb77f6dbf0720538387cce82c61feaa771206600cc7f2aa6bd61b595c08b2fa91b67a60f5d4b1e32f3ce34b6',
        'xrA4E5dVF1jxUHPORbFntxX7uGkla898KG3NfFRxQcI479x94E'
    );



INSERT INTO institute
    (institute_id, address, name, map_pic_folder, map_pic_filename)
    VALUES(
        null, '낙성대 연구공원', '티아카', '/images', 'map_ex.png'
    );

INSERT INTO counselors
    (counselor_id, name, self_intro, career, location,
        institute_id, counsel_method, counsel_target,
        edu_background, type)
    VALUES (
        (SELECT user_id FROM users WHERE nickname='happyit84'),
        '해피나믹',
        '자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.',
        '23423경력입니다. 서울대학교, T아카데미',
        'g343r위치는 지구 어딘가',
        (SELECT institute_id FROM institute WHERE name='티아카'),
        '정신분석학',
        '노인',
        '하버드 대학교 철학과',
        1
    );

INSERT INTO counselors
    (counselor_id, name, self_intro, career, location,
        institute_id, counsel_method, counsel_target,
        edu_background, type)
    VALUES (
        (SELECT user_id FROM users WHERE nickname='choninchon'),
        '할베',
        '자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.',
        '23423경력입니다. 서울대학교, T아카데미',
        'g343r위치는 지구 어딘가',
        (SELECT institute_id FROM institute WHERE name='티아카'),
        '임상병리',
        '중년 여성',
        '경민대학교 소방학과',
        1
    );
INSERT INTO counselors
    (counselor_id, name, self_intro, career, location,
        institute_id, counsel_method, counsel_target,
        edu_background, type)
    VALUES (
        (SELECT user_id FROM users WHERE nickname='chonin'),
        '촌인',
        '자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.자기소개 입니다.',
        '경력입니다. 서울대학교, T아카데미',
        '위치는 지구 어딘가',
        (SELECT institute_id FROM institute WHERE name='티아카'),
        '코칭',
        '대학생',
        '낙성대 상담심리 박사학위',
        1
    );