/**
 * Created by Shin Yeonsik on 2015-05-01.
 */

var wlog = require('../common/wlogger');
var db = require('./db');
var pool = db.pool;
var def = require('../common/def');
var wcrypto = require('../common/wcrypto');
var randstring = require('randomstring');
var conf = require('../common/conf');
var s = require('../control/sessionHelper');


/*
 callback cb param
 err - null, if there's no error
 temp_user_id - inserted temp user id
 */
exports.createTempUser = function(cb){
	//wlog.debug('[model.db_users.createTempUser]');
	pool.getConnection(function(err, conn){
		if(err){
			wlog.error('[model.db_users.createTempUser] FAIL [pool.getConnection]',{err:err});
		}else{
			var sql = 'INSERT INTO users \
				 (user_id, password, acc_conn_type, type, birthday) \
				 values(null, "temp_user_password", 0, 2, "0000-00-00");';
			var data = [];
			conn.query(
				sql,
				data,
				function(err, result){
					var insertId = null;
					if(err){
						wlog.error('[model.db_users.createTempUser]', {sql:sql, data:data});
					}else if(typeof(result.affectedRows) !== 'undefined' &&
						result.affectedRows === 1){
						insertId = result.insertId;
					}

					conn.release();
					cb(err, insertId);
				}
			);
		}
	});
}

/*
callback cb param
	err
	userId - inserted user id
 */
exports.createUser = function(userInfo, cb){
	var logPart = 'model.db_users.createUser';
	var logHead = s.getSessionId()+' ['+logPart+']';
	wcrypto.genPasswordHashSalt(userInfo.password, function(pwHash, salt){
		db.dbDataManipulate(
			'INSERT INTO users ' +
			'(user_id, email, password, nickname, acc_conn_type, type, reg_tm, ' +
			'birthday, gender, salt, pic_folder, pic_file) ' +
			'values(null, ?, ?, ?, 0, 1, now(), ?, ?, ?, ?, ?);',
			[
				userInfo.email,
				pwHash,
				userInfo.nickname,
				userInfo.birthday,
				userInfo.gender,
				salt,
				userInfo.pic_folder,
				userInfo.pic_file
			],
			logPart,
			function(err, success, result){
				if(err){
					wlog.error({err:err}, logHead+' ERROR');
					cb(err, null);
					return;
				}
				cb(err, result.insertId);
			}
		);
	});
}

/*
cb param
	err,
	userId
 */
exports.createUserFacebook = function(userInfo, cb){
	var logPart = 'model.db_users.createUserFacebook';
	var logHead = s.getSessionId()+' ['+logPart+']';
	pool.getConnection(function(err, conn){
		conn.beginTransaction(function(err){
			if(err) {
				wlog.error(logHead + ' ERROR : ', {err: err});
				cb(err, null);
			}else{
				var sql = 'INSERT INTO users (user_id, acc_conn_type, nickname, type, ' +
					'reg_tm, birthday, gender, password, pic_folder, pic_file) values' +
					'(null, 1, ?, 1, now(), ?, ?, "facebook_connected", ?, ?);';
				var values = [
					userInfo.nickname,
					userInfo.birthday,
					userInfo.gender,
					userInfo.pic_folder,
					userInfo.pic_file
				];
				db.dbDataManipulateWithConn(
					conn,	sql, values,
					'model.db_users.createUserFacebook',
					function(err, success, result1){
						if(err){
							wlog.error(logHead+' ERROR : ',{err:err});
							conn.rollback(function(){
								cb(err, false, null);
							});
						}else if( false === success ){
							wlog.error(logHead+' FAILED insertion : ',
								{queryInfo:{sql:sql, values:values}});
							conn.rollback(function(){
								cb(err, false, null);
							});
						}else{
							var sql2 = 'INSERT INTO facebook_login (user_id, facebook_id) ' +
								'values(?, ?);';
							var values2 = [result1.insertId, userInfo.facebook_id];
							db.dbDataManipulateWithConn(
								conn,
								sql2,
								values2,
								'model.db_users.createUserFacebook',
								function(err, success, result2){
									if(err){
										wlog.error(logHead+' ERROR : ',{err:err});
										conn.rollback(function(){
											cb(err, false, null);
										});
									}else{
										conn.commit(function(err){
											if(err){
												conn.rollback(function(){
													cb(err, false, null);
												})
											}else{
												var insertId = result1.insertId;
												cb(err, success, insertId);
											}
										});
									}
								}
							);
						}
					}
				);
			}
		})
	});
}





/*
callback cb param
	err
	exist - true, if email already exists
					false, if email doesn't exist
 */
exports.existEmail = function(email, cb){
	pool.getConnection(function(err, conn){
		if(err){
			wlog.error('[model.db_users.existEmail] ERROR [getConnection] ',{err:err});
			return;
		}

		var queryInfo = {
			sql: 'SELECT count(user_id) dup_count FROM users WHERE email=?',
			values: [email]
		}
		conn.query(queryInfo, function(err, result){
			var exist = false;
			if(err){
				wlog.error({queryInfo:queryInfo}, '[model.db_users.existEmail] ERROR [query] ',{err:err});
			}else{
				if(typeof result.length !== 'undefined' && 1 === result.length){
					if(result[0].dup_count > 0){exist = true;}
					else{exist = false;}
				}else{
					wlog.error({queryInfo:queryInfo}, '[model.db_users.existEmail] ERROR [query / no result] ',{err:err});
					err = new Error('[error from : model.db_users.existEmail]');
					exist = false;
				}
				conn.release();
				cb(err, exist);
			}
		});
	});
}

/*
cb param
	err,
	exist - true, if facebook id already exists
					false, if facebook id doesn't exist
 */
exports.existFacebookId = function(facebookId, cb){
	var logPart = 'model.db_users.existFacebookId';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT count(facebook_id) count FROM facebook_login ' +
			'WHERE facebook_id=?',
		[facebookId],
		logPart,
		function(err, result){
			var exist = false;
			if(err){
				wlog.error(logHead+' ERROR :',{err:err});
				exist = false;
			}else{
				if( 0 == result[0].count ){
					exist = false;
				}else if( 1 == result[0].count ){
					exist = true;
				}else{
					errMsg = new Error('ERROR : id('+facebookId+') count='+result[0].count);
					wlog.error(logHead+errMsg);
					exist = false;
					err = new Error(errMsg);
				}
			}
			cb(err, exist);
		}
	);
}

/*
 callback cb param
	 err
	 exist  - true, if email already exists
	          false, if email doesn't exist
 */
exports.existNickname = function(nickname, cb){
	pool.getConnection(function(err, conn){
		if(err){
			wlog.error('[model.db_users.existNickname] ERROR [getConnection] ',{err:err});
			return;
		}
		var queryInfo = {
			sql: 'SELECT count(user_id) dup_count FROM users WHERE nickname=?',
			values: [nickname]
		}
		conn.query(queryInfo, function(err, result){
			if(err){
				wlog.error({queryInfo:queryInfo}, '[model.db_users.existNickname] ERROR [query] ',{err:err});
				//cb(new Error('[error from : model.db_users.existNickname'), false);
			}else{
				if(typeof result.length !== 'undefined' && 1 === result.length){
					var exist = false;
					if(result[0].dup_count > 0){exist = true;}
					cb(null, exist);
				}else{
					wlog.error({queryInfo:queryInfo},
						'[model.db_users.existNickname] ERROR [query / no result] ',{err:err});
					cb(new Error('[error from : model.db_users.existNickname]'), false);
				}
			}
		});
	});
}



/*
cb param
	err,
	success,
	data = {userId, nickname, passwordHash, salt, isCounselor}
 */
exports.getEmailLoginInfo = function(email, cb){
	var logPart = 'model.db_users.getPasswordHash';
	var logHead = s.getSessionId()+' ['+logPart+']';
	var data = {};
	db.dbSelect(
		'SELECT user_id, nickname, password, salt, pic_folder, pic_file FROM users WHERE email=?',
		[email],
		logPart,
		function(err, result){
			if(err) {
				wlog.error({err: err}, logHead + ' ERROR : email='+email);
				cb(err, null, null);
				return;
			}else if( 0 === result.length ){
				cb(new Error('No Query Result'), null, null);
				return;
			}
			data.userId = result[0].user_id;
			data.passwordHash = result[0].password;
			data.salt = result[0].salt;
			data.name = result[0].nickname;
			data.pic_folder = result[0].pic_folder;
			data.pic_file = result[0].pic_file;
			db.dbSelect(
				'SELECT name FROM counselors WHERE counselor_id=?',
				[data.userId],
				logPart,
				function(err, result){
					if(err){
						wlog.error({err:err}, logHead+' ERROR : Failed to cousleor count');
						cb(err, null, null);
						return;
					}
					if( 0 == result.length ){
						data.isCounselor = false;
					}else if( 1 == result.length ){
						data.isCounselor = true;
						data.name = result[0].name;
					}
					cb(err, true, data);
				}
			);
		}
	);
}

/*
callback signature: cb(err, data = {userId, name, isCounselor)
 */
exports.getFacebookLoginInfo = function(facebookId, cb){
	var logPart = 'model.db_users.getFacebookLoginInfo';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT user_id FROM facebook_login WHERE facebook_id=?',
		[facebookId],
		logPart,
		function(err, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to get user id from facebook id');
				cb(err, null);
				return;
			}else if(result.length !== 1){
				cb(new Error('result.length = '+result.length), null);
				return;
			}
			var userId = result[0].user_id;
			db.dbSelect(
				'SELECT nickname, name counsel_name, pic_folder, pic_file '+
					'FROM (SELECT * FROM users WHERE user_id=?) as u LEFT JOIN counselors '+
					'ON u.user_id = counselors.counselor_id',
				[userId],
				logPart,
				function(err, result2){
					if(err){
						wlog.error({err:err},logHead+' ERROR : failed to get nickname, name');
						cb(err, null);
						return;
					}
					var data = {
						userId: userId,
						profile_pic: conf.domain+result2[0].pic_folder+'/'+result2[0].pic_file
					};
					if( result2[0].counsel_name ){
						data.isCounselor = true;
						data.name = result2[0].counsel_name;
					}else{
						data.isCounselor = false;
						data.name = result2[0].nickname;
					}
					cb(err, data);
					return;
				}
			);
		}
	);
}


/*
callback signature: cb(err, favList)
 */
exports.getFavoriteList = function(loginUserId, cb){
	var logPart = 'common.db_users.getFavorite';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT counselor_id, pic_folder, pic_file, name, birthday, gender, self_intro ' +
		'FROM ' +
		'(SELECT user_id fav_user_id, fav_counselor_id ' +
		'FROM fav_counsel WHERE user_id=?) as f ' +
		'LEFT JOIN counsel_users ON f.fav_counselor_id = counsel_users.counselor_id;',
		[loginUserId],
		logPart,
		function(err, result) {
			if (err) {
				wlog.error({err: err}, logHead + ' ERROR : get favorite list');
				cb(err, null);
				return;
			}
			cb(err, result);
		}
	);
}

/*
callback signature: cb(err, userInfo)
 */
exports.getUserInfo = function(userId, cb){
	var logPart = 'model.db_users.getUserInfo';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT user_id, email, name, acc_conn_type, user_type, birthday, gender, ' +
		'pic_folder, pic_file, self_intro, career, location, institute_id, counsel_method, ' +
		'counsel_target, edu_background, counsel_type ' +
		'FROM user_counselors WHERE user_id=?',
		[userId],
		logPart,
		function(err, results){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to get login user info');
				cb(err, null);
				return;
			}
			if( 1 !== results.length ){
				err = new Error('no query result');
				wlog.error({err:err}, logHead + ' ERROR : no user info / user_id='+userId);
				cb(err, null);
				return;
			}
			cb(err, results[0]);
			return;
		}
	);
}

/*
callback signature: cb(err, password, salt, nickname, type)
 */
exports.getUpdateInfo = function(userId, cb){
	var logPart = 'model.db_users.getUpdateInfo';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT password, salt, nickname, acc_conn_type FROM users WHERE user_id=?',
		[userId],
		logPart,
		function(err, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR');
			}
			if( 1 === result.length ){
				cb(err, result[0].password, result[0].salt, result[0].nickname, result[0].acc_conn_type);
			}else{
				cb(new Error("No Query Result"));
			}
			return;
		}
	);
}

/*
	callback signature: cb(err)
 */
exports.setFavorite = function(userId, counselorId, flag, cb){
	var logPart = 'model.db_users.setFavorite';
	var logHead = s.getSessionId()+' ['+logPart+']';
	if(flag){
		db.dbDataManipulate(
			'INSERT INTO fav_counsel (user_id, fav_counselor_id) VALUES (?,?)',
			[userId, counselorId],
			logPart,
			function(err, success, result){
				if(err){
					if( 'ER_DUP_ENTRY' === err.code ){
						err = null;
					}else{
						wlog.error({err:err}, logHead+' ERROR : fail to add favorite');
					}
				}
				cb(err);
				return;
			}
		);
	}else{
		db.dbDataManipulate(
			'DELETE FROM fav_counsel WHERE user_id=? AND fav_counselor_id=?',
			[userId, counselorId],
			logPart,
			function(err, success, result){
				if(err){
					wlog.error({err:err}, logHead+' ERROR : fail to remove favorite');
				}
				cb(err);
				return;
			}
		);
	}
}

/*
callback signature: cb(err, success)
 */
exports.updateFacebookConnAccount = function(userId, nickname, cb){
	var logPart = 'model.db_users.update';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'UPDATE users SET nickname=? WHERE user_id=?',
		[nickname, userId],
		logPart,
		function(err, success, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR');
				cb(err, false);
				return;
			}
			cb(err, success);
		}
	);
}

/*
callback signature: cb(err, success)
 */
exports.updateNormalAccount = function(loginUserId, password, nickname, cb){
	var logPart = 'model.db_users.update';
	var logHead = s.getSessionId()+' ['+logPart+']';
	wcrypto.genPasswordHashSalt(password, function(passwordHash, salt){
		db.dbDataManipulate(
			'UPDATE users SET password=?, salt=?, nickname=? WHERE user_id=?',
			[passwordHash, salt, nickname, loginUserId],
			logPart,
			function(err, success, result){
				if(err){
					wlog.error({err:err}, logHead+' ERROR');
					cb(err, false);
					return;
				}
				cb(err, success);
			}
		);
	});
}

/*
callback signature: cb(err)
 */
exports.updateProfilePicPath = function(userId, picFolder, picFile, cb){
	var logPart = 'model.db_users.updateProfilePicPath';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'UPDATE users SET pic_folder=?, pic_file=? WHERE user_id=?',
		[picFolder, picFile, userId],
		logPart,
		function(err, success, result){
			if( !err && false === success ){
				err = new Error('query fail');
			}
			if(err){
				wlog.error({err:err}, logHead);
			}
			return cb(err);
		}
	);
}