/**
 * Created by Shin Yeonsik on 2015-05-13.
 */

var db = require('../model/db');
var wlog = require('../common/wlogger');
var async = require('async');
var s = require('../control/sessionHelper');

/*
param
	before  - if null, get most recent messages
						if msg_id, get message before the message
	offset  - number of messages to get

callback signature: cb(err, messages)
 */
exports.getMsg = function(loginUserId, partnerId, before, offset, cb){
	var logPart = 'model.db_msg.getMsg';
	var logHead = s.getSessionId()+' ['+logPart+']';
	var sql = null;
	var values = null;
	if(before){
		sql = 'SELECT * FROM messages ' +
			'WHERE ((sender_id=? AND receiver_id=?) OR (sender_id=? AND receiver_id=?)) ' +
				'AND msg < ? ' +
			'ORDER BY msg_id DESC LIMIT 0, ?';
		values = [loginUserId, partnerId, partnerId, loginUserId, before, offset];
	}else{
		sql = 'SELECT * FROM messages ' +
			'WHERE ((sender_id=? AND receiver_id=?) OR (sender_id=? AND receiver_id=?)) ' +
			'ORDER BY msg_id DESC LIMIT 0, ?';
		values = [loginUserId, partnerId, partnerId, loginUserId, offset];
	}
	db.dbSelect(sql, values, logPart, function(err, messages){
		if(err){
			wlog.error({err:err}, logHead+' ERROR');
		}
		cb(err, messages);
		return;
	});
}

/*
callback signature: cb(err, result)
 */
exports.getRoomList = function(loginUserId, begin, offset, cb){
	var logPart = 'model.db_msg.getRoomList';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT partner_id, last_msg, last_tm, pic_folder, pic_file, name, new \
		FROM (\
			SELECT a2.msg_id msg_id, partner_id, messages.msg last_msg, messages.send_tm last_tm, new \
			FROM(\
				SELECT MAX(msg_id) msg_id, sender_id, receiver_id, IF(sender_id=?, receiver_id, sender_id) partner_id, \
					send_tm last_tm, read_tm, SUM(IF((receiver_id=?) AND (read_tm IS NULL), 1, 0)) new \
				FROM (\
					SELECT * FROM messages WHERE sender_id=? OR receiver_id=? ORDER BY msg_id DESC \
				) AS a \
				GROUP BY partner_id ORDER BY last_tm DESC LIMIT ?,? \
			) AS a2 \
			LEFT JOIN messages \
			ON a2.msg_id=messages.msg_id \
			) AS b \
		LEFT JOIN user_counselors ON b.partner_id=user_counselors.user_id \
		ORDER BY last_tm DESC;',
		[loginUserId, loginUserId, loginUserId, loginUserId, begin, offset],
		logPart,
		function(err, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR');
			}
			cb(err, result);
		}
	);
}

/*
callback signature: cb(err, success, result)
 */
exports.send = function(loginUserId, receiverId, msg, cb){
	var logPart = 'model.db_msg.send';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'INSERT INTO messages (msg_id, sender_id, receiver_id, msg, send_tm) ' +
			'VALUES(null, ?, ?, ?, now());',
		[loginUserId, receiverId, msg],
		logPart,
		function(err, success, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR');
				cb(err, false, null);
				return;
			}
			cb(err, success, result);
		}
	);
}

/*
param:
	readMsgInfo = {
		size: (number-of-read-messages),
		msg_id_array:[(enumeration-of-read-msg_id)]
	}
callback signature: cb(err)
 */
exports.setRead = function(receiverId, msgIds, cb){
	var logPart = 'model.db_msg.setRead';
	var logHead = s.getSessionId()+' ['+logPart+']';
	var sqls = '';
	async.eachSeries(msgIds, function(msgId, cb){
		sqls += 'UPDATE messages SET read_tm=now() ' +
			'WHERE receiver_id='+receiverId+' AND msg_id='+msgId+' AND read_tm IS NULL;';
		cb(null);
	}, function(err){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : async fail')
		}
		db.pool.getConnection(function(err, conn){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : getConnection from pool');
				cb(err);
				return;
			}
			conn.query(sqls,function(err, results){
				if(err){
					wlog.error({err:err}, logHead+' ERROR : query fail');
					cb(err);
					return;
				}
				if(msgIds.length !== results.length ){
					err = new Error('Not all queries are done');
					cb(err);
					return;
				}
				async.each(results, function(result, cb2){
					if( 1 !== result.affectedRows ){
						cb2(new Error('Not all queries are done'));
						return;
					}
					cb2();
				}, function(err){
					cb(err);
				});
			});
		});
	});
}