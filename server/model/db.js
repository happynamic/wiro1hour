/**
 * Created by Shin Yeonsik on 2015-05-06.
 */

var wlog = require('../common/wlogger');
var mysql = require('mysql');
var fs = require('fs');
var s = require('../control/sessionHelper');

/*
options.json EXAMPLE
{
	"connectionLimit":10,
	"host": "localhost",
	"user": "root",
	"password": "1234",
	"database": "wiro1hour"
}


 */

var options = JSON.parse(fs.readFileSync('./model/options.json', 'utf-8'));
options.multipleStatements=true;
exports.pool = mysql.createPool(options);

/*
 cb param
 err
 success - true, insertion succeeded
 false, failed
 result  - result from db query
 */
exports.dbDataManipulate = function(sql, values, logFirstPart, cb){
	var logHead = s.getSessionId()+' ['+logFirstPart+':dbDataManipulate]';
	exports.pool.getConnection(function(err, conn){
		if(err){
			wlog.error(logHead+' ERROR : ',{err:err});
			cb(err, false, null);
		}else {
			exports.dbDataManipulateWithConn(conn, sql, values, logFirstPart, function(err, success, result){
				conn.release();
				cb(err, success, result);
			});
		}
	});
}

/*
 callback signature: cb(err, success, result)
 */
exports.dbDataManipulateWithConn = function(conn, sql, values, logFirst, cb){
	var logHead = s.getSessionId()+' ['+logFirst+':dbDataManipulateWithConn]';
	if(!conn){
		wlog.error(logHead+'ERROR')
		cb(new Error(logHead+' ERROR : connection failed'), false, null);
	}else{
		var queryInfo = {sql:sql, values:values};
		conn.query(queryInfo, function(err, result){
			var success = false;
			if (err) {
				wlog.error({err:err}, {queryInfo:queryInfo}, logHead + ' ERROR ');
				success = false;
			} else if (typeof result.affectedRows === 'undefined') {
				wlog.error({err:err}, {queryInfo:queryInfo}, logHead + ' ERROR ');
				success = false;
			}else if( 1 === result.affectedRows ){
				success = true;
			}
			cb(err, success, result);
		});
	}
}

/*
 process select sql query
 sql
 values
 logHead
 callback signatrue: cb(err, result)
 */
exports.dbSelect = function(sql, values, logFirstPart, cb){
	var logHead = s.getSessionId()+' ['+logFirstPart+':dbSelect]';
	exports.pool.getConnection(function(err, connct){
		if(err){
			wlog.error(logHead+' ERROR : ',{err:err});
			return;
		}
		var queryInfo = {sql:sql, values: values};
		connct.query(queryInfo,
			function(err, result){
				if(err){
					wlog.error({queryInfo:queryInfo}, logHead+' ERROR : ',{err:err});
				}
				connct.release();
				cb(err, result);
			}
		);
	});
};