CREATE VIEW counsel_users AS
SELECT
	counselor_id, name, self_intro, career, location, institute_id,
	counsel_method, counsel_target, edu_background,
	counselors.type counsel_type, user_id,
	email, nickname, acc_conn_type, users.type user_type, reg_tm,
	withdraw_tm, birthday, gender, pic_folder, pic_file, password,
	salt
	from counselors LEFT JOIN users
ON counselors.counselor_id = users.user_id;