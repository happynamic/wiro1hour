-- --------------------------------------------------------
-- 호스트:                          localhost
-- 서버 버전:                        5.5.42-MariaDB-1~trusty-log - mariadb.org binary distribution
-- 서버 OS:                        debian-linux-gnu
-- HeidiSQL 버전:                  9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 테이블 wiro1hour.certification 구조 내보내기
CREATE TABLE IF NOT EXISTS `certification` (
  `cert_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(45) NOT NULL,
  `issue_organ` varchar(45) NOT NULL,
  PRIMARY KEY (`cert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.config 구조 내보내기
CREATE TABLE IF NOT EXISTS `config` (
  `name` varchar(255) NOT NULL,
  `value` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.counselors 구조 내보내기
CREATE TABLE IF NOT EXISTS `counselors` (
  `counselor_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `self_intro` varchar(1000) NOT NULL,
  `career` varchar(500) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `institute_id` int(11) DEFAULT NULL,
  `counsel_method` varchar(45) NOT NULL,
  `counsel_target` varchar(45) NOT NULL,
  `edu_background` varchar(200) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  PRIMARY KEY (`counselor_id`),
  KEY `fk_counselors_users1_idx` (`counselor_id`),
  KEY `fk_counselors_institute1_idx` (`institute_id`),
  CONSTRAINT `fk_counselors_institute1` FOREIGN KEY (`institute_id`) REFERENCES `institute` (`institute_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_counselors_users1` FOREIGN KEY (`counselor_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.counselor_certification 구조 내보내기
CREATE TABLE IF NOT EXISTS `counselor_certification` (
  `counsel_cert_id` int(11) NOT NULL AUTO_INCREMENT,
  `counselor_id` int(11) NOT NULL,
  `certification_cert_id` int(11) NOT NULL,
  PRIMARY KEY (`counsel_cert_id`),
  KEY `fk_counselor_certification_counselors1_idx` (`counselor_id`),
  KEY `fk_counselor_certification_certification1_idx` (`certification_cert_id`),
  CONSTRAINT `fk_counselor_certification_counselors1` FOREIGN KEY (`counselor_id`) REFERENCES `counselors` (`counselor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_counselor_certification_certification1` FOREIGN KEY (`certification_cert_id`) REFERENCES `certification` (`cert_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.counselor_speciality 구조 내보내기
CREATE TABLE IF NOT EXISTS `counselor_speciality` (
  `counselor_id` int(11) NOT NULL,
  `specialty_id` int(11) NOT NULL,
  PRIMARY KEY (`counselor_id`,`specialty_id`),
  KEY `fk_counselor_speciality_counselors1_idx` (`counselor_id`),
  KEY `fk_counselor_speciality_specialty1_idx` (`specialty_id`),
  CONSTRAINT `fk_counselor_speciality_counselors1` FOREIGN KEY (`counselor_id`) REFERENCES `counselors` (`counselor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_counselor_speciality_specialty1` FOREIGN KEY (`specialty_id`) REFERENCES `specialty` (`specialty_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 뷰 wiro1hour.counsel_users 구조 내보내기
-- VIEW 종속성 오류를 극복하기 위해 임시 테이블을 생성합니다.
CREATE TABLE `counsel_users` (
	`counselor_id` INT(11) NOT NULL,
	`name` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`self_intro` VARCHAR(1000) NOT NULL COLLATE 'utf8_general_ci',
	`career` VARCHAR(500) NULL COLLATE 'utf8_general_ci',
	`location` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`institute_id` INT(11) NULL,
	`counsel_method` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`counsel_target` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`edu_background` VARCHAR(200) NULL COLLATE 'utf8_general_ci',
	`counsel_type` TINYINT(4) NOT NULL,
	`user_id` INT(11) NULL COMMENT 'user_id = email address\nmax length of email is 320 (64 + 1 + 255)',
	`email` VARCHAR(250) NULL COLLATE 'utf8_general_ci',
	`nickname` VARCHAR(10) NULL COLLATE 'utf8_general_ci',
	`acc_conn_type` TINYINT(4) NULL,
	`user_type` TINYINT(4) NULL,
	`reg_tm` DATETIME NULL,
	`withdraw_tm` DATETIME NULL,
	`birthday` DATE NULL,
	`gender` CHAR(1) NULL COLLATE 'utf8_general_ci',
	`pic_folder` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`pic_file` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`password` VARCHAR(1024) NULL COLLATE 'utf8_general_ci',
	`salt` VARCHAR(50) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;


-- 테이블 wiro1hour.coupons 구조 내보내기
CREATE TABLE IF NOT EXISTS `coupons` (
  `coupon_id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `spent_sched_id` int(11) DEFAULT NULL,
  `valid_start_tm` datetime NOT NULL,
  `valid_end_tm` datetime NOT NULL,
  PRIMARY KEY (`coupon_id`),
  KEY `fk_coupons_users1_idx` (`user_id`),
  KEY `fk_coupons_schedule1_idx` (`spent_sched_id`),
  CONSTRAINT `fk_coupons_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupons_schedule1` FOREIGN KEY (`spent_sched_id`) REFERENCES `schedule` (`sched_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.facebook_login 구조 내보내기
CREATE TABLE IF NOT EXISTS `facebook_login` (
  `user_id` int(11) NOT NULL,
  `facebook_id` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_table1_users1_idx` (`user_id`),
  CONSTRAINT `fk_table1_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.fav_counsel 구조 내보내기
CREATE TABLE IF NOT EXISTS `fav_counsel` (
  `user_id` int(11) NOT NULL,
  `fav_counselor_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`fav_counselor_id`),
  KEY `fk_fav_counsel_users1_idx` (`user_id`),
  KEY `fk_fav_counsel_counselors1_idx` (`fav_counselor_id`),
  CONSTRAINT `fk_fav_counsel_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fav_counsel_counselors1` FOREIGN KEY (`fav_counselor_id`) REFERENCES `counselors` (`counselor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.forums 구조 내보내기
CREATE TABLE IF NOT EXISTS `forums` (
  `forum_id` int(11) NOT NULL AUTO_INCREMENT,
  `counselor_id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(200) NOT NULL,
  `reg_tm` datetime NOT NULL,
  PRIMARY KEY (`forum_id`),
  KEY `fk_forums_counselors1_idx` (`counselor_id`),
  CONSTRAINT `fk_forums_counselors1` FOREIGN KEY (`counselor_id`) REFERENCES `counselors` (`counselor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.forum_comment 구조 내보내기
CREATE TABLE IF NOT EXISTS `forum_comment` (
  `forum_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_subject_id` int(11) NOT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `reg_tm` datetime DEFAULT NULL,
  `writer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`forum_comment_id`),
  KEY `fk_forum_comment_forum_subject1_idx` (`forum_subject_id`),
  KEY `fk_forum_comment_users1` (`writer_id`),
  CONSTRAINT `FK_forum_comment_forum_subject` FOREIGN KEY (`forum_subject_id`) REFERENCES `forum_subject` (`subject_id`),
  CONSTRAINT `fk_forum_comment_users1` FOREIGN KEY (`writer_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.forum_subject 구조 내보내기
CREATE TABLE IF NOT EXISTS `forum_subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reg_tm` datetime NOT NULL,
  PRIMARY KEY (`subject_id`),
  KEY `fk_forum_writing_forums1_idx` (`forum_id`),
  KEY `fk_forum_writing_users1_idx` (`user_id`),
  CONSTRAINT `fk_forum_writing_forums1` FOREIGN KEY (`forum_id`) REFERENCES `forums` (`forum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_forum_writing_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.gcm_regid 구조 내보내기
CREATE TABLE IF NOT EXISTS `gcm_regid` (
  `regid` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0: uninstalled / 1: installed',
  `reg_tm` datetime NOT NULL,
  PRIMARY KEY (`regid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.institute 구조 내보내기
CREATE TABLE IF NOT EXISTS `institute` (
  `institute_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `map_pic_folder` varchar(45) NOT NULL,
  `map_pic_filename` varchar(255) NOT NULL,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.messages 구조 내보내기
CREATE TABLE IF NOT EXISTS `messages` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `msg` varchar(1000) NOT NULL,
  `send_tm` datetime NOT NULL,
  `read_tm` datetime DEFAULT NULL,
  PRIMARY KEY (`msg_id`),
  KEY `fk_messages_users1_idx` (`sender_id`),
  KEY `fk_messages_users2_idx` (`receiver_id`),
  CONSTRAINT `fk_messages_users1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_users2` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.noti 구조 내보내기
CREATE TABLE IF NOT EXISTS `noti` (
  `noti_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reg_tm` datetime DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL,
  `info` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`noti_id`),
  KEY `fk_noti_users1_idx` (`user_id`),
  CONSTRAINT `fk_noti_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.reviews 구조 내보내기
CREATE TABLE IF NOT EXISTS `reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `counselor_id` int(11) NOT NULL,
  `reg_tm` datetime NOT NULL,
  `text` varchar(10000) NOT NULL,
  `sched_id` int(11) NOT NULL,
  `grade` float NOT NULL,
  PRIMARY KEY (`review_id`),
  KEY `fk_reviews_users1_idx` (`user_id`),
  KEY `fk_reviews_users2_idx` (`counselor_id`),
  KEY `fk_reviews_schedule1_idx` (`sched_id`),
  CONSTRAINT `fk_reviews_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviews_users2` FOREIGN KEY (`counselor_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviews_schedule1` FOREIGN KEY (`sched_id`) REFERENCES `schedule` (`sched_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.schedule 구조 내보내기
CREATE TABLE IF NOT EXISTS `schedule` (
  `sched_id` int(11) NOT NULL AUTO_INCREMENT,
  `counselor_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_tm` datetime NOT NULL,
  `end_tm` datetime NOT NULL,
  `type` tinyint(4) NOT NULL,
  `worry` varchar(200) DEFAULT NULL,
  `talk_method` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`sched_id`),
  KEY `fk_schedule_users1_idx` (`counselor_id`),
  KEY `fk_schedule_users2_idx` (`user_id`),
  CONSTRAINT `fk_schedule_users1` FOREIGN KEY (`counselor_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_schedule_users2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.specialty 구조 내보내기
CREATE TABLE IF NOT EXISTS `specialty` (
  `specialty_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(45) NOT NULL,
  PRIMARY KEY (`specialty_id`),
  UNIQUE KEY `specialty_id_UNIQUE` (`specialty_id`),
  UNIQUE KEY `name_UNIQUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 wiro1hour.users 구조 내보내기
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'user_id = email address\nmax length of email is 320 (64 + 1 + 255)',
  `email` varchar(250) DEFAULT NULL,
  `nickname` varchar(10) DEFAULT NULL,
  `acc_conn_type` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `reg_tm` datetime DEFAULT NULL,
  `withdraw_tm` datetime DEFAULT NULL,
  `birthday` date NOT NULL,
  `gender` char(1) DEFAULT NULL,
  `pic_folder` varchar(255) DEFAULT NULL,
  `pic_file` varchar(255) DEFAULT NULL,
  `password` varchar(1024) NOT NULL,
  `salt` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `nickname_UNIQUE` (`nickname`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 뷰 wiro1hour.user_counselors 구조 내보내기
-- VIEW 종속성 오류를 극복하기 위해 임시 테이블을 생성합니다.
CREATE TABLE `user_counselors` (
	`user_id` INT(11) NOT NULL COMMENT 'user_id = email address\nmax length of email is 320 (64 + 1 + 255)',
	`email` VARCHAR(250) NULL COLLATE 'utf8_general_ci',
	`name` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`acc_conn_type` TINYINT(4) NOT NULL,
	`user_type` TINYINT(4) NOT NULL,
	`withdraw_tm` DATETIME NULL,
	`birthday` DATE NOT NULL,
	`gender` CHAR(1) NULL COLLATE 'utf8_general_ci',
	`pic_folder` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`pic_file` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`password` VARCHAR(1024) NOT NULL COLLATE 'utf8_general_ci',
	`salt` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`self_intro` VARCHAR(1000) NULL COLLATE 'utf8_general_ci',
	`career` VARCHAR(500) NULL COLLATE 'utf8_general_ci',
	`location` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`institute_id` INT(11) NULL,
	`counsel_method` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`counsel_target` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`edu_background` VARCHAR(200) NULL COLLATE 'utf8_general_ci',
	`counsel_type` TINYINT(4) NULL
) ENGINE=MyISAM;


-- 뷰 wiro1hour.counsel_users 구조 내보내기
-- 임시 테이블을 제거하고 최종 VIEW 구조를 생성
DROP TABLE IF EXISTS `counsel_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `counsel_users` AS select `counselors`.`counselor_id` AS `counselor_id`,`counselors`.`name` AS `name`,`counselors`.`self_intro` AS `self_intro`,`counselors`.`career` AS `career`,`counselors`.`location` AS `location`,`counselors`.`institute_id` AS `institute_id`,`counselors`.`counsel_method` AS `counsel_method`,`counselors`.`counsel_target` AS `counsel_target`,`counselors`.`edu_background` AS `edu_background`,`counselors`.`type` AS `counsel_type`,`users`.`user_id` AS `user_id`,`users`.`email` AS `email`,`users`.`nickname` AS `nickname`,`users`.`acc_conn_type` AS `acc_conn_type`,`users`.`type` AS `user_type`,`users`.`reg_tm` AS `reg_tm`,`users`.`withdraw_tm` AS `withdraw_tm`,`users`.`birthday` AS `birthday`,`users`.`gender` AS `gender`,`users`.`pic_folder` AS `pic_folder`,`users`.`pic_file` AS `pic_file`,`users`.`password` AS `password`,`users`.`salt` AS `salt` from (`counselors` left join `users` on((`counselors`.`counselor_id` = `users`.`user_id`)));


-- 뷰 wiro1hour.user_counselors 구조 내보내기
-- 임시 테이블을 제거하고 최종 VIEW 구조를 생성
DROP TABLE IF EXISTS `user_counselors`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_counselors` AS select `users`.`user_id` AS `user_id`,`users`.`email` AS `email`,ifnull(`users`.`nickname`,`counselors`.`name`) AS `name`,`users`.`acc_conn_type` AS `acc_conn_type`,`users`.`type` AS `user_type`,`users`.`withdraw_tm` AS `withdraw_tm`,`users`.`birthday` AS `birthday`,`users`.`gender` AS `gender`,`users`.`pic_folder` AS `pic_folder`,`users`.`pic_file` AS `pic_file`,`users`.`password` AS `password`,`users`.`salt` AS `salt`,`counselors`.`self_intro` AS `self_intro`,`counselors`.`career` AS `career`,`counselors`.`location` AS `location`,`counselors`.`institute_id` AS `institute_id`,`counselors`.`counsel_method` AS `counsel_method`,`counselors`.`counsel_target` AS `counsel_target`,`counselors`.`edu_background` AS `edu_background`,`counselors`.`type` AS `counsel_type` from (`users` left join `counselors` on((`users`.`user_id` = `counselors`.`counselor_id`))) where (`users`.`type` = 1);
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
