/**
 * Created by Shin Yeonsik on 2015-05-18.
 */

var wlog = require('../common/wlogger');
var db = require('../model/db');
var s = require('../control/sessionHelper');

/*
callback signature: cb(err, success, forumId)
 */
exports.createForum = function(counselorId, title, desc, cb){
	var logPart = 'model.db_forum.create';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'INSERT INTO forums (forum_id, counselor_id, title, description, reg_tm) '+
			'VALUES (null, ?, ?, ?, now());',
		[counselorId, title, desc],
		logPart,
		function(err, success, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : fail to create forum');
				cb(err, false, null);
				return;
			}
			cb(err, success, result.insertId);
			return;
		}
	);
}

/*
callback signature: cb(err, success, subejctId)
 */
exports.createForumSubject = function(userId, forumId, firstComment, cb){
	var logPart = 'model.db_forum.createForumSubject';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.pool.getConnection(function(err, conn){
		if(err) {
			wlog.error(logHead + ' ERROR : ', {err: err});
			cb(err, null);
		}else {
			var sql = 'INSERT INTO forum_subject (subject_id, forum_id, user_id, reg_tm) VALUES '+
				'(null, ?, ?, now())';
			var values = [forumId, userId];
			db.dbDataManipulateWithConn(conn, sql, values, logPart, function(err, success, result1){
				if(err){
					wlog.error({err:err}, logHead+' ERROR : failed to create new subject');
					conn.rollback(function(){
						cb(err, false, null);
					});
					return;
				}
				if( !success ){
					wlog.error(logHead+' ERROR : failed to create new subject 2');
					conn.rollback(function(){
						cb(err, false, null);
					});
					return;
				}
				var sql2 =  'INSERT INTO forum_comment ' +
					'(forum_comment_id, forum_subject_id, text, writer_id, reg_tm) '+
					' VALUES(null, ?, ?, ?, now())';
				var values2 = [result1.insertId, firstComment, userId];
				db.dbDataManipulateWithConn(conn, sql2, values2, logPart, function(err, sucess, result2){
					if(err){
						wlog.error({err:err}, logHead+' ERROR : failed to create first comment');
						conn.rollback(function(){
							cb(err, false, null);
						});
						return;
					}
					if( !success ){
						wlog.error({err:err}, logHead+' ERROR : failed to create first comment 2');
						conn.rollback(function(){
							cb(err, false, null);
						});
						return;
					}
					conn.commit(function(err){
						if(err){
							wlog.error({err:err}, logHead+' ERROR : failed to commit');
							conn.rollback(function(){
								cb(err, false, null);
							});
							return;
						}
						cb(err, success, result1.insertId);
						return;
					});
				});
			});
		}
	});
}

/*
callback signature: cb(err, forumList={})
 */
exports.getForumList = function(cb){
	var logPart = 'model.db_forum.getForumList';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT forum_id, counselor_id, counsel_name, pic_folder, pic_file, title, ' +
			'description, subject_id, a.writer_id, count(forum_comment.writer_id) participant_num ' +
		'FROM	(SELECT b.forum_id forum_id, counselor_id, counsel_name, pic_folder, ' +
			'pic_file, title, description, subject_id, user_id writer_id ' +
			'FROM (SELECT forum_id, counsel_users.counselor_id counselor_id, name counsel_name, ' +
						'pic_folder, pic_file, title, description FROM forums LEFT JOIN counsel_users ' +
						'ON forums.counselor_id=counsel_users.counselor_id) AS b ' +
			'LEFT JOIN forum_subject ON b.forum_id=forum_subject.forum_id) AS a ' +
		'LEFT JOIN forum_comment ON a.subject_id=forum_comment.forum_subject_id ' +
		'GROUP BY forum_id ORDER BY forum_id DESC',
		[],
		logPart,
		function(err, forumList){
			if(err){
				wlog.error({err:err}, logHead+' ERROR');
			}
			cb(err, forumList);
		}
	);
}

/*
callback signature: cb(err, forumId, counselorId, counselorName, clientId)
 */
exports.getInfoForForumNoti = function(subjectId, cb){
	var logPart = 'model.db_forum.getForumIdCounselorId';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT forum_id, sf.counselor_id counselor_id, name counsel_name, subject_id, user_id client_id ' +
		'FROM (SELECT forums.forum_id forum_id, counselor_id, title, description, reg_tm, subject_id, user_id ' +
			'FROM forums INNER JOIN ' +
				'(SELECT subject_id, forum_id, user_id ' +
				'FROM forum_subject WHERE subject_id=?) AS s ' +
			'ON forums.forum_id=s.forum_id) AS sf ' +
		'INNER JOIN counselors ' +
		'ON sf.counselor_id=counselors.counselor_id;',
		[subjectId],
		logPart,
		function(err, results){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : get forum_id, counselor_id using subject_id');
				return cb(err, null, null, null);
			}
			if( 1 !== results.length ){
				err = new Error('no query result / subject_id='+subjectId);
				wlog.error({err:err}, ' ERROR');
				return cb(err, null, null, null);
			}
			return cb(err, results[0].forum_id, results[0].counselor_id, results[0].counsel_name, results[0].client_id);
		}
	);
}



/*
callback signature: cb(err, forum, subjects, comments)
 */
exports.getSubjectList = function(forumId, begin, offset, userId, cb){
	var logPart = 'model.db_forum.getSubjectList';
	var logHead = s.getSessionId()+' ['+logPart+']';
	var userCondition = (userId ? 'AND user_id='+userId : ' ');
	db.dbSelect(
		'SELECT * FROM forums WHERE forum_id=?;',
		[forumId],
		logPart,
		function(err, forums){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to get forum info');
				cb(err, null, null, null);
				return;
			}
			if( !forums || forums.length < 1 ){
				err = new Error('No query result');
				wlog.error({err:err}, logHead+' ERROR : no query result, get forum info');
				cb(err, null, null, null);
				return;
			}
			db.dbSelect(
				'SELECT *, count(*) comment_num ' +
				'FROM (SELECT * FROM forum_subject WHERE forum_id=? '+userCondition+') AS a ' +
				'LEFT JOIN forum_comment ON a.subject_id=forum_comment.forum_subject_id ' +
				'GROUP BY a.subject_id ORDER BY a.subject_id DESC;',
				[forumId],
				logPart,
				function(err, subjects){
					if(err){
						wlog.error({err:err}, logHead+' ERROR : failed to get subject info');
						cb(err, null, null, null);
						return;
					}
					db.dbSelect(
						'SELECT forum_subject_id, forum_comment_id, text, reg_tm, writer_id, ' +
							'name, birthday, gender, pic_folder, pic_file, IF(counsel_type=1, true, false) counselor_comment '+
						'FROM (SELECT forum_comment_id, forum_subject_id, text, f2.reg_tm, writer_id '+
							'FROM (SELECT forum_subject_id, forum_comment_id, text, forum_comment.reg_tm, writer_id '+
								'FROM forum_comment ' +
								'INNER JOIN (SELECT * FROM forum_subject WHERE forum_id=? '+userCondition+' ORDER BY subject_id LIMIT ?, ?) AS f1 '+
								'ON forum_comment.forum_subject_id=f1.subject_id) AS f2 '+
								'WHERE	(SELECT COUNT(*) FROM forum_comment AS f '+
									'WHERE f.forum_subject_id = f2.forum_subject_id AND '+
										'f.forum_comment_id <= f2.forum_comment_id) <= 2 '+
								'ORDER BY forum_subject_id, forum_comment_id) AS g '+
								'LEFT JOIN user_counselors ON g.writer_id=user_counselors.user_id;',
						[forumId, begin, offset],
						logPart,
						function(err, comments){
							if(err){
								wlog.error({err:err}, logHead+' ERROR : failed to get comments');
								cb(err, null, null, null);
								return;
							}
							cb(err, forums[0], subjects, comments);
							return;
						}
					);
				}
			);
		}
	);
}

/*
cb signature: cb(err, subject, comments)
 */
exports.getComments = function(subjectId, cb){
	var logPart = 'model.db_forum.getComments';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT * FROM (SELECT * FROM forum_subject WHERE subject_id=?) AS f ' +
			'INNER JOIN forums ON f.forum_id=forums.forum_id;',
		[subjectId],
		logPart,
		function(err, subjects){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to get subject info');
				cb(err, null, null);
				return;
			}
			if( subjects.length !== 1 ){
				err = new Error('No subejct');
				wlog.error({err:err}, logHead+' ERROR : subject_id='+subjectId);
				cb(err, null, null);
				return;
			}
			db.dbSelect(
				'SELECT *, IF(u.counsel_type=1, true, false) counselor_comment '+
				'FROM (SELECT * FROM forum_comment WHERE forum_subject_id=?) AS f '+
				'LEFT JOIN (SELECT user_id, name, pic_folder, pic_file, counsel_type FROM user_counselors) AS u '+
				'ON f.writer_id=u.user_id '+
				'ORDER BY forum_comment_id LIMIT 0, 100;',
				[subjectId],
				logPart,
				function(err, comments){
					if(err){
						wlog.error({err:err}, logHead+' ERROR : failed to get comments');
						cb(err, subjects[0], null);
						return;
					}
					if( 0 === comments.length ){
						err = new Error('zero comment');
						wlog.error({err:err}, logHead+' ERROR : zero comments, subject_id='+subjectId);
						cb(err, subjects[0], comments);
						return;
					}
					cb(err, subjects[0], comments);
					return;
				}
			);
		}
	);
}

/*
callback signature: cb(err, commentId)
 */
exports.writeComment = function(subjectId, comment, writerId, cb){
	var logPart = 'model.db_forum.writeComment';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'INSERT INTO forum_comment (forum_comment_id, forum_subject_id, text, reg_tm, writer_id) '+
			'VALUES(null, ?, ?, now(), ?)',
		[subjectId, comment, writerId],
		logPart,
		function(err, success, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to write comment');
				cb(err, false, null);
				return;
			}
			if( success ){
				cb(err, result.insertId);
			}else{
				cb(err, null);
			}
			return;
		}
	);
}