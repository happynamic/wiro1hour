/**
 * Created by Shin Yeonsik on 2015-05-20.
 */

var db = require('../model/db');
var def = require('../common/def');
var wlog = require('../common/wlogger');
var s = require('../control/sessionHelper');

/*
callback signature: cb(err)
 */
exports.addGcmRegId = function(regId, userId, cb){
	var logPart = 'model.db_gcmRegId.addGcmRegId';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'INSERT INTO gcm_regid (regid, user_id, type, reg_tm)' +
		'VALUES (?, ?, ?, now())',
		[regId, userId, def.GCM_REGID_TYPE.INSTALLED],
		logPart,
		function(err, success, results){
			if(err){
				if( 'ER_DUP_ENTRY' !== err.code ){
					wlog.error({err:err}, logHead+' ERROR : add gcm regid');
					cb(err);
					return;
				}else{
					updateGcmRegId(regId, userId, def.GCM_REGID_TYPE.INSTALLED, function(err){
						if(err){
							wlog.error({err:err}, logHead+' ERROR : update gcm regid');
						}
						cb(err);
						return;
					});
				}
			}else{
				if(success){
					cb(err);
				}else{
					err = new Error('fail to gcm regid add query');
					cb(err);
				}
				return;
			}
		}
	);
}

/*
call it when the app is being uninstalled

callback signature: cb(err)
 */
exports.disableGcmRegId = function(regId, cb){
	var logHead = s.getSessionId()+' [model.db_gcmregId.disableGcmRegId]';
	updateGcmRegId(regId, null, def.GCM_REGID_TYPE.UNINSTALLED, function(err){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : disable gcm reg id');
		}
		cb(err);
	});
}

/*
callback signature: cb(err)
 */
exports.getRegIds = function(userId, cb){
	var logPart = 'model.db_gcmRegId.getRegIds';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT regid FROM gcm_regid WHERE user_id=?',
		[userId],
		logPart,
		function(err, results){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : get regIds / user_id='+userId);
			}
			cb(err, results);
		}
	);
}

/*
callback signature: cb(err)
 */
var updateGcmRegId = function(regId, userId, type, cb){
	var logPart = 'model.db_gcmregId.updateGcmRegId';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbDataManipulate(
		'UPDATE gcm_regid SET user_id=?, type=? WHERE regid=?',
		[userId, type, regId],
		logPart,
		function(err, success, results){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : update regid');
			}
			cb(err);
		}
	);
}
