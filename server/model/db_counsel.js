/**
 * Created by Shin Yeonsik on 2015-05-12.
 */

var wlog = require('../common/wlogger');
var db = require('../model/db');
var async = require('async');
var random = require('random-js')(); // 뒤에 양괄호가 꼭 붙어야
var errJson = require('../common/errJson');
var s = require('../control/sessionHelper');


/*
callback signature: cb(err, counselor)
 */
exports.automatch = function(cb){
	var logPart = 'model.db_counsel.automatch';
	var logHead = s.getSessionId()+' ['+logPart+']';
	/***************************************************************************
	 * 임시적으로 랜덤으로 결과 출력: 알고리즘 변경 필요
	 ***************************************************************************/
	db.dbSelect(
		'SELECT counselor_id, name, pic_folder, pic_file, self_intro FROM counsel_users',
		[],
		logPart,
		function(err, counselList){
			if(err){
				wlog.error({err:err},logHead+' ERROR : select counselors');
				cb(err, null);
				return;
			}else{
				if( 0 === counselList.length ){
					err = new Error('query no result');
					cb(err, null);
					return;
				}else{
					var matchIndex = random.integer(0, counselList.length-1);
					var counselor = counselList[matchIndex];
					cb(err, counselor);
				}
			}
		}
	);
}

/*
cb signature: cb(err, list = {})
 */
exports.getList = function(loginUserId, cb){
	var logPart = 'model.db_counsel.getList';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT ' +
			'counselor_id, pic_folder, pic_file, name, birthday, gender, self_intro ' +
			'FROM counselors LEFT JOIN users ON counselors.counselor_id = users.user_id',
		[],
		logPart,
		function(err, counselorList){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : dbSelect counselor list');
				cb(err, null);
				return;
			}
			if(loginUserId){
				db.dbSelect(
					'SELECT fav_counselor_id FROM fav_counsel WHERE user_id=?',
					[loginUserId],
					logPart,
					function(err, favList){
						if(err){
							wlog.error({err:err}, logHead+' ERROR : dbSelect favoriate');
							cb(err, null);
							return;
						}
						async.each(counselorList, function(counsel, eachCB){
							counsel.favorite = false;
							async.detect(favList, function existCounsel(fav, detectCB){
								if( fav.fav_counselor_id === counsel.counselor_id ){
									detectCB(true);
								}
							}, function setFav(foundFav){
								if( foundFav ){
									counsel.favorite = true;
								}
							});
							eachCB(null);
						}, function(err){
							cb(err, counselorList);
							return;
						});
					}
				);
			}else{
				async.each(counselorList, function(counsel, eachCB){
					counsel.favorite = false;
					eachCB(null);
				}, function eachCB(err){
					cb(err, counselorList);
					return;
				});
			}
		}
	);
}

/*
callback signature: cb(err, profile)
 */
exports.getProfile = function(loginUserId, counselorId, cb){
	logPart = 'model.db_counsel.getProfile';
	logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT counselor_id, pic_folder, pic_file, counsel_name, birthday, gender, '+
			'location, self_intro, career, a.institute_id institute_id, counsel_method, '+
			'counsel_target, edu_background, address, institute.name institute_name, '+
			'map_pic_folder, map_pic_filename '+
			'FROM '+
				'(SELECT counselor_id, pic_folder, pic_file, counselors.name counsel_name, '+
					'birthday, gender, location, self_intro, career, institute_id, '+
					'counsel_method, counsel_target, edu_background '+
					'FROM counselors LEFT JOIN users '+
					'ON counselors.counselor_id = users.user_id WHERE counselor_id=?) as a '+
				'LEFT JOIN institute '+
			'ON a.institute_id = institute.institute_id;',
		[counselorId],
		logPart,
		function(err, result){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : get profie counselor_id='+counselorId);
				cb(err, null);
				return;
			}
			if(0 === result.length){
				err = new Error('No Query Result');
				wlog.error({err:err}, logHead+' ERROR : no query result');
				cb(err, result);
				return;
			}
			var profile = result[0];
			if( loginUserId ){
				db.dbSelect(
					'SELECT 1 FROM fav_counsel WHERE user_id=? AND fav_counselor_id=?',
					[loginUserId, counselorId],
					logPart,
					function(err, fav){
						if(err){
							wlog.error({err:err},
								logHead+' ERROR : get favorite user_id='+loginUserId+
								', fav_counselor_id='+counselorId);
							cb(err, null);
							return;
						}
						if( 1 === fav.length ){
							profile.favorite = true;
						}else{
							profile.favorite = false;
						}
						cb(err, profile);
					}
				);
			}else{
				profile.favorite = false;
				cb(err, profile);
			}
		}
	);
}

/*
param
	counselorId
	profile = {self_intro, counsel_method, counsel_target, location, career, edu_background}

callback signature: cb(err, success)
 */
exports.updateProfile = function(counselorId, profile, cb){
	var logPart = 'model.db_counsel.updateProfile';
	var logHead = s.getSessionId()+' ['+logPart+']';
	db.dbSelect(
		'SELECT self_intro, counsel_method, counsel_target, location, career, edu_background ' +
		'FROM counselors WHERE counselor_id=?',
		[counselorId],
		logPart,
		function(err, results){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to get old profile');
				cb(err, false);
				return;
			}
			if( 1 !== results.length ){
				err = new Error('no query result');
				wlog.error({err:err}, logHead+' ERROR : no old profile');
				cb(err, false);
				return;
			}
			var old = results[0];
			var self_intro = profile.self_intro || old.self_intro;
			var counsel_method = profile.counsel_method || old.counsel_method;
			var counsel_target = profile.counsel_target || old.counsel_target;
			var location = profile.location || old.location;
			var career = profile.career || old.career;
			var edu_background = profile.edu_background || old.edu_background;
			db.dbDataManipulate(
				'UPDATE counselors ' +
				'SET self_intro=?, counsel_method=?, counsel_target=?, location=?, career=?, edu_background=? ' +
				'WHERE counselor_id=?',
				[self_intro, counsel_method, counsel_target, location, career, edu_background, counselorId],
				logPart,
				function(err, success, results2){
					if(err){
						wlog.error({err:err}, logHead+' ERROR : update counselor profile');
						cb(err, false);
						return;
					}
					cb(err, success);
					return;
				}
			);
		}
	);
}