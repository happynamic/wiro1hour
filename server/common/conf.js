/**
 * Created by Shin Yeonsik on 2015-05-08.
 * desc: configuration file manager
 *  you can read configuration file
 */

/* conf.json (example)
{
	"domain": "http://www.wiro1hour.com"
}
 */


var fs = require('fs');
var wlog = require('../common/wlogger');

var conf = JSON.parse(fs.readFileSync('./conf.json', 'utf-8'));

wlog.debug('[common.conf] conf=',JSON.stringify(conf));

// set default values for non-existing value
if( typeof conf.domain === 'undefined' ){
	conf.domain = 'http://www.wiro1hour.com';
}

module.exports = conf;