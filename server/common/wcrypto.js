/**
 * Created by Shin Yeonsik on 2015-05-08.
 * crypto module
 */


var crypto = require('crypto');
var randstring = require('randomstring');
var def = require('../common/def');

/*
callback signature: cb(passwordHash, salt)
 */
exports.genPasswordHashSalt = function(plainPw, cb){
	var salt = randstring.generate(def.SALT_LENGTH);
	var passwordHash = exports.hashPassword(plainPw, salt);
	cb(passwordHash, salt);
}

/*
create hash
	plainPw - plain password
 */
exports.hashPassword = function(plainPw, salt){
	return crypto.createHash('sha512').update(plainPw + salt).digest('hex');
}