/**
 * Created by Shin Yeonsik on 2015-05-20.
 */

var KOREAN = {
	NOTI_FORUM_SUBJECT_CREATION_TITLE: '포럼',
	NOTI_FORUM_SUBJECT_CREATION_DESC : '포럼에 주제가 생성되었습니다.',
	NOTI_FORUM_COMMENT_TITLE: '포럼',
	NOTI_FORUM_COMMENT_DESC: '포럼에 답변이 달렸습니다.'
};



module.exports = KOREAN;