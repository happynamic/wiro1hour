/**
 * Created by Shin Yeonsik on 2015-05-07.
 */

//var ip = require('ip');

/*exports.domain = function(){
	var addr = ip.address();
	return "http://"+addr+":3000";
}*/

var wlog = require('../common/wlogger');

exports.getAgeGroup = function(birthday){
	return Math.floor(((new Date()).getYear() - birthday.getYear())/10)*10;
}

// add 9 hours to time
exports.transTime = function(t){
	/*var t_toISOString = t.toISOString();
	var t1 = new Date(t.setHours(t.getHours()+9));
	var t2 = t1.toISOString();
	var s_toDateString = t1.toDateString();
	var s_toGMTString = t1.toGMTString();
	var s_toISOString = t1.toISOString();
	var s_toJSON = t1.toJSON();
	var s_toLocaleDateString = t1.toLocaleDateString();
	var s_toLocaleString = t1.toLocaleString();
	var s_toLocaleTimeString = t1.toLocaleTimeString();
	var s_toString = t1.toString();
	var s_toTimeString = t1.toTimeString();
	var s_toUTCString = t1.toUTCString();
	var t3 = t2.replace('Z', '+0900');
	*/
	return new Date(t.setHours(t.getHours()+9)).toISOString().replace('Z', '+0900');
}

exports.resjson = function(req, res, json, debugLog){
	var r = res.json(json);
	if(debugLog){
		wlog.debug('[RESPONSE] ['+req.method+'] '+req.originalUrl +' / res.json='+JSON.stringify(json));
	}
	return r;
}