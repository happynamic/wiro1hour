/**
 * Created by Shin Yeonsik on 2015-05-09.
 */
var fbgraph = require('fbgraph');
var wlog = require('../common/wlogger');
var s = require('../control/sessionHelper');

/*
 cb param
 err, facebookId
 */
exports.getFacebookId = function(accessToken, cb){
	var logHead = s.getSessionId()+' [common.facebook.getFacebookId]';
	fbgraph.setAccessToken(accessToken);
	fbgraph.get('me', function(err, data){
		if(err){
			wlog.error(logHead+' ERROR : ',{err:err});
			cb(err, null);
		}else if( typeof data.id === 'undefined' ){
			wlog.error(logHead+' ERROR [id is undefined] : ',{err:err});
			cb(err, null);
		}else{
			cb(err, data.id);
		}
	});
}