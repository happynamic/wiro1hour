/**
 * Created by Shin Yeonsik on 2015-05-06.
 */

exports.ERR_RESERVED            = {code:0, msg:"reserved error code"};
exports.ERR_GENERNAL            = {code:1, msg:"general fail"};
exports.ERR_NEED_LOGIN          = {code:2, msg:"need to login"};
exports.ERR_DUP_EMAIL           = {code:3, msg:"duplicated email"};
exports.ERR_WRONG_PARAM         = {code:4, msg:"wrong parameter"};
exports.ERR_NO_EMAIL            = {code:5, msg:"non-existing email"};
exports.ERR_NO_ID               = {code:6, msg:"non-existing id"};
exports.ERR_NEED_COUNSEL_LOGIN  = {code:7, msg:"need to login with counselor account"};
exports.ERR_WRONG_EMAIL_PASSSWORD = {code:8, msg:"wrong email or wrong password"};
exports.ERR_QUERY_NO_RESULT     = {code:9, msg:"query returned no result"};
exports.ERR_WRONG_PASSWORD      = {code:10, msg:"wrong password"};
exports.ERR_PERMISSION_NOT_GRANTED = {code:11, msg:"permission is not granted"};
exports.ERR_INVALID_BIRTHDAY    = {code:12, msg:"invalid birthday"};
exports.ERR_RENAME              = {code:13, msg:"rename failure"};
exports.ERR_DB_QUERY            = {code:14, msg:"db query failure"};
exports.ERR_MKDIR               = {code:15, msg:"mkdir failure"};
exports.ERR_INVALID_REGID         = {code:16, msg:"invalid regid"};