/**
 * Created by Shin Yeonsik on 2015-05-01.
 */

var winston = require('winston');
var moment = require('moment');
var moment_tz = require('moment-timezone');
var fs = require('fs');
var async = require('async');

moment_tz.tz('Asia/Seoul');

// There are 6 default levels in winston:
// silly=0(lowest), debug=1, verbose=2, info=3, warn=4, error=5(highest)
var wlog = new (winston.Logger)({
	transports:[
		/*new (winston.transports.Console)({
			level: 'error',
			colorize: true;
		}),*/
		new winston.transports.DailyRotateFile({
			level: 'silly',
			filename: './log/wiro1hour',
			maxsize: 1048576, // 1024*1024
			datePattern: '.yyyy-MM-dd.log',
			timestamp: function(){return moment().format('YYYY-MM-DD HH:mm:ss.SSS');}
		})
	]
});

fs.mkdir('log', function(e){
	if(!e || (e && e.code === 'EEXIST')){
		logPath = './log';
		wlog.info('[common/wlogger.js] winston logger initialized');
	}
});

module.exports = wlog;