/**
 * Created by Shin Yeonsik on 2015-05-08.
 * validate necessary types
 */

var validator = require('validator');
var def = require('../common/def');

exports.isEmail = function(email){
	return (
		email.length <= def.VALIDATE.EMAIL_MAX_LEN &&
		validator.isEmail(email)
	);
}

exports.isNickname = function(nickname){
	return (
		typeof nickname !== 'undefined' &&
		nickname.length <= def.VALIDATE.NICKNAME_MAX_LEN
	);
}

exports.isResisterUserType = function(type){
	return (
		typeof type !== 'undefined' &&
		(def.ACC_TYPE.NORMAL == type)
	);
}

exports.isDate = function(date){
	return validator.isDate(date);
}

exports.isAccConnType = function(accConnType){
	return (
		typeof accConnType !== 'undefined' &&
		(def.ACC_CONN_TYPE.NONE == accConnType || def.ACC_CONN_TYPE.FACEBOOK == accConnType)
	);
}
