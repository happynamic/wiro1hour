/**
 * Created by Shin Yeonsik on 2015-05-08.
 */



// account type
exports.ACC_TYPE = {
	NORMAL: 1,
	TEMP: 2
};

// account connection type
exports.ACC_CONN_TYPE = {
	NONE: 0,
	FACEBOOK: 1
}

exports.LOGIN_TYPE = {
	EMAIL_PASSWORD: 1,
	FACEBOOK: 2
}

// Talk Method Defintion
exports.TALK_METHOD = {
	FACE_TO_FACE: 1,
	VIDEO_CALL: 2,
	VOICE_CALL: 3,
	CHATTING: 4
};

// salt length for password of account
exports.SALT_LENGTH = 50;

exports.GCM_REGID_TYPE = {
	UNINSTALLED: 0,
	INSTALLED: 1
}

exports.GCM_TYPE = {
	CHAT_MSG: 1,
	FORUM_SUBJECT_CREATION: 2,
	FORUM_COMMENT: 3
}

exports.NOTI = {
	FORUM_SUBJECT_CREATION: 2,
	FORUM_COMMENT: 3
}

exports.FILE = {
	PROFILE_DEFAULT_PATH: '/images',
	PROFILE_DEFAULT_M: 'profile_default_M.png',
	PROFILE_DEFAULT_F: 'profile_default_F.png'
}

exports.DB = {
	GENDER_MALE: 'M',
	GENDER_FEMALE: 'F'
}

exports.VALIDATE = {
	EMAIL_MAX_LEN: 250,
	NICKNAME_MAX_LEN: 10
}