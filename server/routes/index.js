var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//[GET] /email_auth/:rand_string
router.get('/email_auth/:rand_string', function(req, res, next) {
	res.send(req.url);
});

module.exports = router;
