/**
 * Created by Shin Yeonsik on 2015-04-30.
 */

var express = require('express');
var validator = require('validator');
var wlog = require('../common/wlogger');
var errJson = require('../common/errJson');
var userCtrl = require('../control/userCtrl');
var db_users = require('../model/db_users');
var def = require('../common/def');
var conf = require('../common/conf');
var wvalidator = require('../common/wvalidator');
var counselCtrl = require('../control/counselCtrl');
var sessHelp = require('../control/sessionHelper');
var msgCtrl = require('../control/msgCtrl');
var forumCtrl = require('../control/forumCtrl');
var gcmCtrl = require('../control/gcmCtrl');
var db_gcmRegId = require('../model/db_gcmRegId');
var notiCtrl = require('../control/notiCtrl');

var router = express.Router();

/*
url from: /api
 */



// [GET] /api/automatch
router.get('/automatch', function(req, res, next) {
	counselCtrl.automatch(req, res, next);
});

// [GET] /api/chat/list/:begin/:offset
router.get('/chat/list/:begin/:offset', function(req, res, next) {
	msgCtrl.getRoomList(req, res, next);
});

// [GET] /api/chat/room/:partner_id
router.get('/chat/room/:partner_id', function(req, res, next) {
	msgCtrl.getMsg(req, res, next);
});

// [POST] /api/chat/send/:partner_id
router.post('/chat/send/:partner_id', function(req, res, next) {
	msgCtrl.send(req, res, next);
});

// [PUT] /api/chat/set_read
router.put('/chat/set_read', function(req, res, next) {
	msgCtrl.setRead(req, res, next);
});

// [GET] /api/counselor_list
router.get('/counselor_list', function(req, res, next) {
	counselCtrl.list(req, res, next);
});

// [GET] /api/coupons/available
router.get('/coupons/available', function(req, res, next) {
	res.json({
		success:true,
    data:{
			size: 2,
	    coupons:[
				{
          coupon_id:232,
					title: "쿠폰 타이틀",
					valid_start_tm: "2015-04-01",
					valid_end_tm: "2015-04-30"
				},
				{
					coupon_id:234,
					title: "쿠폰 타이틀",
					valid_start_tm: "2015-04-01",
					valid_end_tm: "2015-04-30"
				}
			]
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [GET] /api/favorite
router.get('/favorite', function(req, res, next) {
	userCtrl.getFavoriteList(req, res, next);
});

// [GET] /api/email_dup/:email
router.get('/email_dup/:email', function(req, res, next) {
	if(typeof req.params.email === 'undefined' ||
		false === validator.isEmail(req.params.email)){
		wlog.error('[routes.api./email_dup/:email (GET)] ERROR [WRONG email address] url='+req.url);
		res.json({success:false, error:errJson.ERR_WRONG_PARAM});
		return;
	}

	db_users.existEmail(req.params.email, function(err, exist){
		if(err){
			wlog.error({err:err}, '[routes.api./email_dup/:email (GET)] ERROR [email='+req.params.email);
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}

		res.json({
			success: true,
			data:{email_dup:exist}
		});
	});
});

// [PUT] /api/favorite/add/:counselor_id
router.post('/favorite/add/:counselor_id', function(req, res, next) {
	userCtrl.setFavorite(req, res, next, true);
});

// [PUT] /api/favorite/remove/:counselor_id
router.delete('/favorite/remove/:counselor_id', function(req, res, next) {
	userCtrl.setFavorite(req, res, next, false);
});

// [GET] /api/forum
router.get('/forum', function(req, res, next) {
	forumCtrl.getForumList(req, res, next);
});

// [POST] /api/forum
router.post('/forum', function(req, res, next) {
	forumCtrl.createForum(req, res, next);
});

// [GET] /api/forum/subject/:subject_id
router.get('/forum/subject/:subject_id', function(req, res, next) {
	forumCtrl.getComments(req, res, next);
});

// [POST] /api/forum/subject
router.post('/forum/subject', function(req, res, next) {
	forumCtrl.createForumSubject(req, res, next);
});

// [POST] /api/forum/subject/:subject_id
router.post('/forum/subject/:subject_id', function(req, res, next) {
	forumCtrl.writeComment(req, res, next);
});

// [GET] /api/forum/view/:forum_id
router.get('/forum/view/:forum_id', function(req, res, next) {
	forumCtrl.getAllSubjectList(req, res, next);
});

// [GET] /api/forum/view/:forum_id/mine
router.get('/forum/view/:forum_id/mine', function(req, res, next) {
	forumCtrl.getMySubjectList(req, res, next);
});

// [POST] /api/gcm
router.post('/gcm', function(req, res, next){
	gcmCtrl.sendGcmTest(req, res, next);
});

// [POST] /api/init_password
router.post('/init_password', function(req, res, next) {
	res.json({success:true,
		dummy_server:"현재값은 더미 서버 값"});
});

// [POST] /api/login
router.post('/login', function(req, res, next) {
	userCtrl.login(req, res, next);
});

// [GET] /api/logout
router.get('/logout', function(req, res, next) {
	var logHead = sessHelp.getSessionId()+' [api.get.logout]';
	sessHelp.getGcmRegId(req.session, function(regId){
		db_gcmRegId.addGcmRegId(regId, null, function(err){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : disable reg id');
			}
			sessHelp.logout(req.session, function(err){
				res.json({success:true});
			});
		});
	});
});

// [GET] /api/nickname_dup/:nickname
router.get('/nickname_dup/:nickname', function(req, res, next) {
	if( typeof req.params.nickname === 'undefined' /*||
			false === wvalidator.isNickname(req.params.nickname)*/ ){
		wlog.error(
			'[routes/api/nickname_dup/:nickname (GET)] WRONG FORMAT / nickname='+
			req.params.nickname);
		res.json({success:false, error:errJson.ERR_WRONG_PARAM});
		return;
	}

	db_users.existNickname(req.params.nickname, function(err, exist){
		if(err){
			wlog.error({err:err},
				'[routes/api/nickname_dup/:nickname (GET)] ERROR / nickname='
				+req.params.nickname);
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}

		res.json({
			success: true,
			data:{nickname_dup:exist}
		});
		return;
	});
});

// [GET] /api/noti
router.get('/noti', function(req, res, next) {
	notiCtrl.getNotiList(req, res, next);
});

// [PUT] /api/noti
router.put('/noti', function(req, res, next) {
	notiCtrl.setNotiRead(req, res, next);
});

// [PUT] /api/page
router.put('/page', function(req, res, next){
	counselCtrl.updateProfile(req, res, next);
});

// [GET] /api/page/:counselor_id
router.get('/page/:counselor_id', function(req, res, next) {
	counselCtrl.profile(req, res, next);
});

// [POST] /api/profile_pic
router.post('/profile_pic', function(req, res, next) {
	userCtrl.updateProfilePic(req, res, next);
});

// [POST] /api/regid
router.post('/regid', function(req, res, next){
	gcmCtrl.addRegId(req, res, next);
});

// [PUT] /api/regid
router.put('/regid', function(req, res, next){
	res.json({success:true, dummy_server:"현재값은 더미 데이터"});
});

// [GET] /api/review/:counselor_id/:begin/:offset
router.get('/review/:counselor_id/:begin/:offset', function(req, res, next) {
	res.json({
		success:true,
    data:{
			size: 1,
	    reviews:[
				{
					review_id: 1001,
					reg_tm: "2015-03-30",
					grade: 4,
					text: "정말 좋았습니다."
				}
			]
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [POST] /api/review/:sched_id
router.post('/review/:sched_id', function(req, res, next) {
	res.json({success:true});
});

// [GET] /api/sched/available/:counselor_id/:year/:mon
router.get('/sched/available/:counselor_id/:year/:mon', function(req, res, next) {
	res.json({
		success: true,
    data:{
			size: 1,
	    schedules:[
				{
					start_tm: "2015-05-03 20:30:00",
					end_tm: "2015-05-03 21:30:00"
				}
			]
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [GET] /api/sched/counselor/:year/:mon
router.get('/sched/counselor/:year/:mon', function(req, res, next) {
	res.json({
		success: true,
    data:{
			size: 1,
	    schedules:[
		    {
			    date: "yyyy-mm-dd",
			    start_time: "HH:MM:SS",
			    end_time: "HH:MM:SS",
			    reserved: true,
			    reservation_info: {
				    user_id: 1,
				    nickname: "만득이",
				    talk_method: 4
			    }
		    }
			]
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [GET] /api/sched/done
router.get('/sched/done', function(req, res, next) {
	res.json({
		success: true,
    data:{
			size: 1,
	    schedules:[
		    {
			    sched_id: 2,
			    counselor_pic: conf.domain+'/images/profile_ex.jpg',
			    counselor_name: "상담사 이름",
			    client_pic: conf.domain+'/images/profile_ex.jpg',
			    client_nickname: "내담자 닉네임",
			    start_tm: "2015-06-08 18:30",
			    minutes: 60,
			    talk_method: def.TALK_METHOD.FACE_TO_FACE,
          review_writable: true
				}
			]
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [PUT] /api/sched/reserve
router.put('/sched/reserve', function(req, res, next) {
	res.json({
    success: true,
    data:{
			size: 1,
	    schedules:[
				{
					sched_id: 1,
					profile_pic: "상담사 프로필 사진 url",
					name: "상담사 이름",
					start_tm: "yyyy-mm-dd HH:MM:SS",
					minutes: 30,
					talk_method: 1
				}
			]
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [GET] /api/sched/reserved
router.get('/sched/reserved', function(req, res, next) {
	wlog.silly('[ACCESS] /api/sched/reserved [GET]');
	res.json({
		success: true,
    data:{
			size: 1,
	    schedules:[
				{
          sched_id: 2,
					counselor_pic: conf.domain+'/images/profile_ex.jpg',
					counselor_name: "상담사 이름",
					client_pic: conf.domain+'/images/profile_ex.jpg',
					client_nickname: "내담자 닉네임",
					start_tm: "2015-06-08 18:30",
					minutes: 60,
					talk_method: def.TALK_METHOD.FACE_TO_FACE
				}
			]
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [GET] /api/sched/reserved/:sched_id
router.get('/sched/reserved/:sched_id', function(req, res, next) {
	res.json({
		success: true,
		data:{
			name: "상담사 이름",
			date: "yyyy-dd-mm",
			time: "HH:MM-HH:MM",
			coupon_title: "1일 이용권",
			talk_method: 4
		},
		dummy_server:"현재값은 더미 서버 값"
	});
});

// [PUT] /api/sched/unreserve
router.put('/sched/unreserve', function(req, res, next) {
	res.json({success:true,
		dummy_server:"현재값은 더미 서버 값"});
});


// [GET] /api/user
router.get('/user', function(req, res, next){
	userCtrl.getLoginUserInfo(req, res, next);
});

// [POST] /api/user
router.post('/user', function(req, res, next) {
	userCtrl.register(req, res, next);
});

// [PUT] /api/user
router.put('/user', function(req, res, next) {
	wlog.debug({req_body:req.body});//debug
	userCtrl.update(req, res, next);
	//res.json({success:true,
	//	dummy_server:"현재값은 더미 서버 값"});
});


// 테스트를 위해서 만든 나중에 반드시 지워야함 [GET] /api/genvcmowvjhjkwlwfewfthjs
router.get('/genvcmowvjhjkwlwfewfthjs', function(req, res, next){
	var db = require('../model/db');
	db.dbDataManipulate('DELETE FROM facebook_login',[],'api.get.genvcmowvjhjkwlwfewfthjs', function(err, success, result){
		if(err){
			res.json({success:false, msg:'failed to delete all the facebook accounts'});
			return;
		}
		res.json({success:true, msg:'All the facebook connected accouts are deleted.\nYOU MUST DELETE THIS API BEFORE YOU RELEASE THE SERVER.'});
		return;
	});
});

// 테스트를 위해서 만든, 나중에 반드시 지워야함 [GET] /api/remove_all_regid
router.get('/remove_all_regid', function(req, res, next){
	var db = require('../model/db');
	db.dbDataManipulate('DELETE FROM gcm_regid', [], 'api.get.remove_all_regid', function(err, success, result){
		if(err){
			res.json({success:false, msg:'failed to delete all the reg IDs'});
			return;
		}
		res.json({success:true, msg:'All the reg IDs are deleted. YOU MUST DELETE THIS API BEFORE YOU RELEASE THE SERVER.'});
		return;
	});
});

module.exports = router;
