/**
 * Created by Shin Yeonsik on 2015-05-14.
 */

// TEST CODE
// node test1.js > output.txt


var request = require('request').defaults({jar:true}); // request에서 세션을 유지 시키려면 defaults({jar:true}) 필요
var async = require('async');
//var req = require('supertest');
var randstring = require('randomstring');

var test = function(func){func()};

var head = 'http://localhost:3000';

//reqest = req(head);

async.waterfall([
	function(cb){
		var url = head+'/api/automatch';
		request(url, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		var url = head+'/api/chat/room/3';
		request(url, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		var url = head+'/api/counselor_list';
		request(url, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		var url = head+'/api/chat/send/4';
		postParam = {
			url:url,
			form:{msg:'test'}
		};
		request.post(postParam, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null, res.headers);
		});
	},
	function(headers, cb){
		var url = head+'/api/chat/send/6';
		var postParam = {
			url:url,
			form:{msg:'test'}
		};
		request.post(postParam, function(err, res, body){
			console.log('#### '+url+' ####\nparam='+JSON.stringify(postParam.form)+'\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		var url = head+'/api/chat/room/8';
		request(url, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	///////////////////////// LOGIN ///////////////////////////////
	function(cb){
		var url = head+'/api/login';
		var postParam = {
			url:url,
			form:{
				type: 1,
				email: 'shin_yun_sik@hanmail.net',
				password: 'password',
				regid: 'test regid'
			}
		};
		request.post(postParam, function(err, res, body){
			console.log('#### '+url+' ####\nparam='+JSON.stringify(postParam.form)+'\n'+body+'\n\n');

			async.waterfall([
				function(cb2){
					var url = head+'/api/user';
					request({url:url}, function(err, res, body){
						console.log('#### '+url+' ####(login)\n'+body+'\n\n');
						cb2(null);
					});
				},
				function(cb2){
					var url = head+'/api/chat/list/0/100';
					request({url:url}, function(err, res, body){
						console.log('#### '+url+' ####(login)\n'+body+'\n\n');
						cb2(null);
					});
				},
				function(cb2){
					var url = head+'/api/user';
					putParam = {
						url:url,
						form:{
							old_password: 'password',
							new_password: 'password1',
							nickname: 'password1'
						}
					};
					request.put(putParam, function(err, res, body){
						console.log('#### '+url+' ####(login, update1)\n'+body+'\n\n');

						var url = head + '/api/user';
						putParam = {
							url: url,
							form: {
								old_password: 'password1',
								new_password: 'password',
								nickname: 'password'
							}
						};
						request.put(putParam, function (err, res, body) {
							console.log('#### ' + url + ' ####(login, update1, update2)\n' + body + '\n\n');
							cb2(null);
						});
					});
				},
				function(cb2){
					var url = head+'/api/chat/room/8';
					request(url, function(err, res, body){
						console.log('#### '+url+'####(login)\n'+body+'\n\n');
						cb2(null);
					});
				},
				function(cb2){
					var url = head+'/api/chat/room/8?begin=3&limit_last=1';
					request(url, function(err, res, body){
						console.log('#### '+url+'####(login)\n'+body+'\n\n');
						cb2(null);
					});
				},
				function(cb2){
					var url = head+'/api/chat/set_read';
					var putParam = {
						url:url,
						form:{
							read_msg: '{"size":2,"msg_id_array":[2,3]}'
						}
					};
					request.put(putParam, function(err, res, body) {
						console.log('#### ' + url + ' ####(login)\nparam=' + JSON.stringify(putParam.form) + '\n' + body + '\n\n');
						cb2(null);
					});
				},
				function(cb2){
					var url = head+'/api/chat/send/8';
					var postParam = {
						url:url,
						form:{
							msg: 'TEST '+randstring.generate(10)
						}
					};
					request.post(postParam, function(err, res, body) {
						console.log('#### ' + url + ' ####(login)\nparam=' + JSON.stringify(postParam.form) + '\n' + body + '\n\n');
						cb2(null);
					});
				},
				function(cb2){
					// 포럼에서 내 소주제 목록 보기
					var url = head+'/api/forum/view/1/mine';
					request({url:url}, function(err, res, body){
						console.log('#### '+url+' ####(login)\n'+body+'\n\n');
						cb2(null);
					});
				},
				function(cb2){
					// 로그인한 상태로 포럼에서 소주제 열람
					var url = head+'/api/forum/subject/2';
					request({url:url}, function(err, res, body){
						console.log('#### '+url+' ####(login)\n'+body+'\n\n');
						cb2(null);
					});
				},
				function(cb2){
					// 알림 보기
					var url = head+'/api/noti';
					request({url:url}, function(err, res, body){
						console.log('#### '+url+' ####(login)\n'+body+'\n\n');
						cb2(null);
					});
				}
				// ADD HERE FOR TESTING IN LOG-IN STATUS
			], function (err){
				cb(null);
			});
		});
	}	,
	function(cb){
		var url = head+'/api/chat/list/0/100';
		request({url:url}, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	/////////////////////// LOGOUT //////////////////////////////
	function(cb){
		var url = head+'/api/logout';
		request({url:url}, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},//
	function(cb){
		// 포럼 리스트 받기
		var url = head+'/api/forum';
		request(url, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		var url = head+'/api/chat/room/8';
		request({url:url}, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		// 포럼 보기
		var url = head+'/api/forum/view/1';
		request({url:url}, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		// 포럼에서 내 소주제 보기
		var url = head+'/api/forum/view/1/mine';
		request({url:url}, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		// 로그인 안한 상태로 포럼에서 소주제 열람
		var url = head+'/api/forum/subject/2';
		request({url:url}, function(err, res, body){
			console.log('#### '+url+' ####\n'+body+'\n\n');
			cb(null);
		});
	},
	function(cb){
		// 상담사로 로그인 /////////////////////////////////////////////////
		var url = head+'/api/login';
		var postParam = {
			url:url,
			form:{
				type: 1,
				email: 'happyit@happynamic.com',
				password: 'password'
			}
		};
		request.post(postParam, function(err, res, body) {
			console.log('#### ' + url + ' ####\nparam=' + JSON.stringify(postParam.form) + '\n' + body + '\n\n');
			async.waterfall([
				function(cb2){
					// 상담사 프로필 업데이트
					var url = head+'/api/page';
					var putParam = {
						url:url,
						form:{
							self_intro: 'self intro UPDATE TEST '+randstring.generate(10),
							location: 'location UPDATE TEST'+randstring.generate(10)
						}
					};
					request.put(putParam, function(err, res, body) {
						console.log('#### ' + url + ' ####(counsel_login)\nparam=' + JSON.stringify(putParam.form) + '\n' + body + '\n\n');
						cb2(null);
					});
				},
				function(cb2){
					// 알림 테스트
					var url = head+'/api/noti';
					request(url, function(err, res, body){
						console.log('#### ' + url + ' ####(counsel_login)\n' + body + '\n\n');
						cb2(null);
					});
				}
			], function (err) {
				cb(null);
			});
		});
	}
],
function(err,results){
	console.log('TEST END');
});