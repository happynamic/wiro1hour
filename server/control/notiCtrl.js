/**
 * Created by Shin Yeonsik on 2015-05-19.
 */

var db_noti = require('../model/db_noti');
var sessHelp = require('../control/sessionHelper');
var errJson = require('../common/errJson');
var wlog = require('../common/wlogger');
var db_forum = require('../model/db_forum');
var def = require('../common/def');
var stringTable = require('../common/stringTable');
var async = require('async');
var common = require('../common/common');

/*
callback signature: cb(err)
 */
exports.addNotiForForumSubjectCreation = function(subjectId, cb){
	var logHead = sessHelp.getSessionId()+' [control.notiCtrl.addNotiForForumSubjectCreation]';
	db_forum.getInfoForForumNoti(subjectId, function(err, forumId, counselorId, counselorName, clientId){
		db_noti.addNoti(
			def.NOTI.FORUM_SUBJECT_CREATION,
			counselorId,
			{
				forum_id: forumId,
				subject_id: subjectId,
				title: stringTable.NOTI_FORUM_SUBJECT_CREATION_TITLE,
				desc: stringTable.NOTI_FORUM_SUBJECT_CREATION_DESC
			},
			function(err, notiId){
				if(err){
					wlog.error({err:err}, logHead+' ERROR');
				}
				cb(err);
			});
	});
}

/*
callback signature: cb(err)
 */
exports.addNotiForForumComment = function(writerId, subjectId, commentId, cb) {
	var logHead = sessHelp.getSessionId()+' [control.notiCtrl.addNotiForForumComments]';
	db_forum.getInfoForForumNoti(subjectId, function (err, forumId, counselorId, counselorName, clientId) {
		var destUserId = null;
		if (writerId === counselorId) {
			destUserId = clientId;
		} else if (writerId === clientId) {
			destUserId = counselorId;
		} else {
			err = new Error('writerId(=' + writerId + ') is neither counselorId(=' + counselorId + ') nor client(=' + clientId + ')');
			wlog.error({err: err}, logHead + ' ERROR');
			cb(err);
			return;
		}
		db_noti.addNoti(
			def.NOTI.FORUM_COMMENT,
			destUserId,
			{
				forum_id: forumId,
				subject_id: subjectId,
				comment_id: commentId,
				title: stringTable.NOTI_FORUM_COMMENT_TITLE,
				desc: stringTable.NOTI_FORUM_COMMENT_DESC
			},
			function (err, notiId) {
				if (err) {
					wlog.error({err: err}, logHead + ' ERROR');
				}
				cb(err);
			}
		);
	});
}

exports.getNotiList = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.notiCtrl.getNotiList]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		db_noti.getNotiList(loginUserId, function(err, notiList){
			if(err){
				wlog.error({err:err}, logHead);
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			var resNotiList = [];
			async.eachSeries(notiList, function(noti, cb){
				var temp = JSON.parse(noti.info);
				temp.noti_id = noti.noti_id;
				temp.reg_date = common.transTime(noti.reg_tm);
				temp.check = (noti.checked ? true : false);
				temp.type = noti.type;
				resNotiList.push(temp);
				cb(null);
			}, function(err){
				if(err){
					wlog.error({err:err}, logHead);
					res.json({success:false, error:errJson.ERR_GENERNAL});
					return;
				}
				res.json({
					success:true,
					data:{
						size: resNotiList.length,
						noties: resNotiList
					}
				});
				return;
			});
		});
	});
}

exports.setNotiRead = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.notiCtrl.setNotiRead]';
	db_noti.setNotiRead(req.body.noti_id, function(err){
		if(err) {
			wlog.error({err:err}, logHead);
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}
		res.json({success:true});
		return;
	});
}