/**
 * Created by Shin Yeonsik on 2015-05-17.
 */

var wlog = require('../common/wlogger');
var db_forum = require('../model/db_forum');
var errJson = require('../common/errJson');
var sessHelp = require('../control/sessionHelper');
var async = require('async');
var conf = require('../common/conf');
var gcmCtrl = require('../control/gcmCtrl');
var notiCtrl = require('../control/notiCtrl');
var common = require('../common/common');

exports.createForum = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.createForum]';
	sessHelp.getLoginUserInfo(req.session, function(loginUserId, isCounselor){
		if( !loginUserId ){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}else if( !isCounselor ){
			res.json({success:false, error:errJson.ERR_NEED_COUNSEL_LOGIN});
			return;
		}
		db_forum.createForum(loginUserId, req.body.title, req.body.desc, function(err, success, forumId){
			if(err){
				wlog.error({err:err}, logHead+' ERROR');
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			if(!success){
				wlog.error({err:err}, logHead+' ERROR');
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			res.json({success:true, data:{forum_id:forumId}});
			return;
		});
	});
}

exports.createForumSubject = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.createForumSubject]';
	sessHelp.getLoginUserInfo(req.session, function(loginUserId, isCounselor) {
		if (!loginUserId) {
			res.json({success: false, error: errJson.ERR_NEED_LOGIN});
			return;
		}
		db_forum.createForumSubject(
			loginUserId,
			req.body.forum_id,
			req.body.comment,
			function(err, success, subjectId){
				if(err){
					wlog.error({err:err}, logHead+' ERROR : failed to create forum subject');
					res.json({success:false, error:errJson.ERR_GENERNAL});
					return;
				}
				if(success){
					notiCtrl.addNotiForForumSubjectCreation(subjectId, function(err){
						wlog.error({err:err}, logHead+' ERROR : add noti for forum subejct creation');
					});
					gcmCtrl.sendGcmForForumSubjectCreation(subjectId, function(err){
						wlog.error({err:err}, logHead+' ERROR : send gcm for forum subejct creation');
					});
					res.json({success:true, data:{subject_id:subjectId}});
				}else{
					res.json({success:false, error:errJson.ERR_GENERNAL});
				}
				return;
			}
		);
	});
}

exports.getForumList = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.getForumList]';
	db_forum.getForumList(function(err, forumList){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : get forum list');
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}
		var resForumList = [];
		async.eachSeries(forumList, function(forum, cb){
			resForumList.push({
				forum_id: forum.forum_id,
				pic_path: conf.domain+forum.pic_folder+'/'+forum.pic_file,
				title: forum.title,
				desc: forum.description,
				counselor_id: forum.counselor_id,
				counselor_name: forum.counsel_name,
				participant_num: forum.participant_num
			});
			cb(null);
		}, function(err){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : build response json');
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			res.json({
				success: true,
				data:{
					size: resForumList.length,
					fora: resForumList
				}
			});
			return;
		});
	});
}

exports.getAllSubjectList = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.getSubjectList]';
	var begin = parseInt(req.query.begin || 0);
	var offset = parseInt(req.query.offset || 100);
	var forumId = parseInt(req.params.forum_id);
	getSubjectList(forumId, begin, offset, null, function(err, resData){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : get subject list');
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}
		res.json({success:true, data:resData});
		return;
	});
}

exports.getComments =function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.getComments]';
	var subjectId = null;
	if( req.params.subject_id ){
		subjectId = parseInt(req.params.subject_id);
	}else{
		res.json({success:false, error:errJson.ERR_WRONG_PARAM});
		return;
	}
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		db_forum.getComments(req.params.subject_id, function(err, subject, comments){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to get comments subject='+req.params.subject_id);
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			var resComments = [];
			async.eachSeries(comments, function(comment, cb){
				resComments.push({
					comment_id: comment.forum_comment_id,
					counselor_comment: (comment.counselor_comment? true : false),
					user_id: comment.user_id,
					profile_pic: conf.domain+comment.pic_folder+'/'+comment.pic_file,
					name: comment.name,
					comment: comment.text,
					reg_tm: common.transTime(comment.reg_tm)
				});
				cb(null);
			}, function(err){
				var writable = false;
				if( loginUserId && (loginUserId === subject.user_id || loginUserId === subject.counselor_id) ){
					writable = true;
				}
				var resData = {
					writable: writable,
					size: resComments.length,
					comments: resComments
				};
				res.json({success:true, data:resData});
				return;
			});
		});
	});
}

exports.getMySubjectList = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.getMySubjectList]';
	var begin = parseInt(req.query.begin || 0);
	var offset = parseInt(req.query.offset || 100);
	var forumId = parseInt(req.params.forum_id);
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			wlog.error(logHead+' ERROR : need to login');
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		getSubjectList(forumId, begin, offset, loginUserId, function(err, resData){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : get subject list');
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			res.json({success:true, data:resData});
			return;
		});
	});
}

/*
callback signature: callback(err, resData)
 */
var getSubjectList = function(forumId, begin, offset, userId, callback){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.getSubjectList]';
	db_forum.getSubjectList(
		forumId,
		begin,
		offset,
		userId,
		function(err, forum, subjects, comments){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : get info from db');
				callback(err, null);
				return;
			}
			// insert subjects array into map, mapping with subject id
			var resSubjects = [];
			async.eachSeries(subjects, function(subject, cb){
				resSubjects.push({
					subject_id: subject.subject_id,
					total_comment_num: subject.comment_num,
					size: 0,
					comments: []
				});
				cb(null);
			}, function(err){
				if(err){
					wlog.error({err:err}, logHead+' ERROR : failed to build up response json 1');
					callback(err, null);
					return;
				}
				async.each(comments, function(comment, cb){
					async.select(resSubjects, function(subject, selectCB){
						if( comment.forum_subject_id === subject.subject_id ){
							subject.comments.push({
								counselor_comment: (comment.counselor_comment ? true : false),
								user_id: comment.writer_id,
								name: comment.name,
								profile_pic: conf.domain+comment.pic_folder+'/'+comment.pic_file,
								comment: comment.text,
								reg_tm: common.transTime(comment.reg_tm)
							});
							subject.size++;
							selectCB(true);
						}
					}, function(results){});
					cb(null);
				}, function(err){
					if(err){
						wlog.error({err:err}, logHead+' ERROR : failed to build up response json 2');
						callback(err, null);
						return;
					}
					var resData = {
						forum_id: forumId,
						title: forum.title,
						desc: forum.description,
						subjects_num: resSubjects.length,
						subjects: resSubjects
					};
					callback(null, resData);
					return;
				});
			});
		}
	);
}

exports.writeComment = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.forumCtrl.writeComment]';
	var subjectId = parseInt(req.params.subject_id);
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		db_forum.writeComment(subjectId, req.body.comment, loginUserId, function(err, commentId){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : failed to write comment');
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			if( !commentId ){
				err = new Error('failed to write comment');
				wlog.error({err:err}, logHead+' ERROR : failed to write comment 2');
				return;
			}
			notiCtrl.addNotiForForumComment(loginUserId, subjectId, commentId, function(err){
				if(err) wlog.error({err:err}, logHead+' ERROR : add noti for forum comment');
			});
			gcmCtrl.sendGcmForForumComment(loginUserId, subjectId, commentId, function(err){
				if(err) wlog.error({err:err}, logHead+' ERROR : send gcm for forum comment');
			});
			res.json({success:true});
			return;
		});
	});
}