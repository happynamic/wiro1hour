/**
 * Created by Shin Yeonsik on 2015-05-01.
 */

var wlog = require('../common/wlogger');

var sessionId = null;
exports.setSessionId = function(paramSessionId){
	sessionId = paramSessionId;
}
exports.getSessionId = function(){return sessionId;}

exports.sessFirstAccess = function(session){};


/*
callback signature: cb(loginUserId)
	loginUserId - null, if no one is login
								login user id - if a user logged in
 */
exports.getLoginUserId = function(session, cb){
	var loginUserId = session.loginUserId || null;
	if( typeof session.loginUserId === 'undefined' ){
			loginUserId = null;
	}else{
		loginUserId = session.loginUserId;
	}
	cb(loginUserId);
}

/*
callback signature: cb(loginUserId, isCounselor)
 */
exports.getLoginUserInfo = function(session, cb){
	var loginUserId = session.loginUserId || null;
	var isCounselor = session.isCounselor || null;
	cb(loginUserId, isCounselor);
}

/*
write session for login
cb param
	err,
 */
exports.login = function(session, userId, isCounselor, gcmRegId, cb){
	session.loginUserId = userId;
	session.isCounselor = isCounselor;
	session.gcmRegId = gcmRegId;
	wlog.info('[LOGIN] user_id='+userId);
	cb(null);
}

/*
set loginUserId to null
cb param
	err
 */
exports.logout = function(session, cb){
	if( typeof session.loginUserId !== 'undefined' ){
		wlog.info('[LOGOUT] user_id='+session.loginUserId);
	}
	session.loginUserId = null;
	session.isCounselor = null;
	session.gcmRegId = null;
	cb(null);
}

/*
callback siganture: cb(gcmRegId)
 */
exports.getGcmRegId = function(session, cb){
	cb(session.gcmRegId);
}

/*
callback signature: cb()
 */
exports.setGcmRegId = function(session, regId, cb){
	session.gcmRegId = regId;
	cb();
};