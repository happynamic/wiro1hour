/**
 * Created by Shin Yeonsik on 2015-05-13.
 */

var db_msg = require('../model/db_msg');
var sessHelp = require('../control/sessionHelper');
var errJson = require('../common/errJson');
var wlog = require('../common/wlogger');
var async = require('async');
var conf = require('../common/conf');
var gcmCtrl = require('../control/gcmCtrl');
var common = require('../common/common');


exports.getMsg = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.msgCtrl.getMsg]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		var partner_id = parseInt(req.params.partner_id);
		var before = null;
		if( typeof req.query.before !== 'undefined' ){
			before = parseInt(req.query.before);
		}
		var limit_last = 100;
		if( typeof req.query.limit_last !== 'undefined' ){
			limit_last = parseInt(req.query.limit_last);
		}
		db_msg.getMsg(
			loginUserId,
			partner_id,
			before,
			limit_last,
			function(err, messages){
				if(err){
					wlog.error({err:err}, ' ERROR');
					res.json({success:false, error:errJson.ERR_NEED_LOGIN});
					return;
				}
				var resMessages = [];
				async.eachSeries(messages, function(msg, cb){
					/*var d = new Date(msg.send_tm);
					var h = d.getHours();
					d.setHours(h+9);*/
					resMessages.unshift({
						msg_id: msg.msg_id,
						sender_id: msg.sender_id,
						receiver_id: msg.receiver_id,
						msg: msg.msg,
						send_tm: common.transTime(msg.send_tm),
						read: msg.read_tm ? true : false
					});
					cb(null);
				}, function(err){
					if(err){
						wlog.error({err:err}, logHead+' ERROR');
						res.json({success:false, error:errJson.ERR_GENERNAL});
						return;
					}
					res.json({
						success:true,
						data:{
							size: resMessages.length,
							messages: resMessages
						}
					});
				});
			}
		);
	});
}

exports.getRoomList = function(req, res, next){
	var begin = parseInt(req.params.begin);
	var offset = parseInt(req.params.offset);
	var logHead = sessHelp.getSessionId()+' [control.msgCtr.getRoomList]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if( !loginUserId ){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}else{
			db_msg.getRoomList(loginUserId, begin, offset, function(err, roomList){
				if(err){
					wlog.error({err:err}, logHead);
					res.json({success:false, error:errJson.ERR_GENERNAL});
					return;
				}
				var chatRooms = []; // rooms for response
				async.eachSeries(roomList, function(room, cb){
					chatRooms.push({
						partner_id: room.partner_id,
						partner_pic: conf.domain+room.pic_folder+'/'+room.pic_file,
						partner_nickname: room.name,
						last_msg: room.last_msg,
						last_chat_tm: common.transTime(room.last_tm),
						new_msg_num: room.new
					});
					cb(null);
				}, function(err){
					if(err){
						wlog.error({err:err}, logHead+' ERROR async eachSeries');
						res.json({success:false, error:errJson.ERR_GENERNAL});
						return;
					}
					res.json({
						success: true,
						data:{
							size: chatRooms.length,
							chat_rooms: chatRooms
						}
					});
				});
			});
		}
	});
}


exports.send = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.chatCtrl.send]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if( !loginUserId ){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}else{
			var partnerId = req.params.partner_id;
			db_msg.send(
				loginUserId,
				partnerId,
				req.body.msg,
				function(err, success, result){
					if(err){
						wlog.error({err:err}, logHead+' ERROR');
						res.json({success:false, error:errJson.ERR_GENERNAL});
						return;
					}
					if(success){
						gcmCtrl.sendGcmForChatMsg(loginUserId, partnerId, result.insertId, function(err){
							wlog.error({err:err}, logHead+' ERROR : send gcm for chat msg');
						});
						res.json({success:true,	data:{msg_id:result.insertId}});
						return;
					}else{
						res.json({success:false, error:errJson.ERR_GENERNAL});
						return;
					}
				}
			);
		}
	});
}

/*
callback signature: cb(err, success)
 */
exports.setRead = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.msgCtrl.setRead]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		var readMsgInfo = JSON.parse(req.body.read_msg);
		if(!readMsgInfo){
			res.json({success:false, error:errJson.ERR_WRONG_PARAM});
			return;
		}
		db_msg.setRead(loginUserId, readMsgInfo.msg_id_array, function(err){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : read msg info=', req.body.read_msg);
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			res.json({success:true});
		});
	});
}