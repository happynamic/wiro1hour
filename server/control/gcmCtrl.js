/**
 * Created by Shin Yeonsik on 2015-05-20.
 */

var db_gcmRegId = require('../model/db_gcmRegId');
var errJson = require('../common/errJson');
var wlog = require('../common/wlogger');
var conf = require('../common/conf');
var gcm = require('node-gcm');
var def = require('../common/def');
var db_forum = require('../model/db_forum');
var async = require('async');
var db_users = require('../model/db_users');
var s = require('../control/sessionHelper');

exports.addRegId = function(req, res, next){
	var logHead = s.getSessionId()+' [control.gcmCtrl.addRegIdUserId]';
	db_gcmRegId.addGcmRegId(req.body.regid, null, function(err){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : add gcm reg id');
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}
		res.json({success:true});
		return;
	});
}

exports.disableRegId = function(req, res, next){
	var logHead = s.getSessionId()+' [control.gcmCtrl.disableRegId]';
	db_gcmRegId.disableGcmRegId();
}

/*
callback signature: cb(err, result)
 */
exports.sendGcm = function(regIds, serverKey, key, msg, cb){
	var logHead = s.getSessionId()+' [control.gcmCtrl.sendGcm]';
	var gcmMsg = new gcm.Message();
	gcmMsg.addData(key, msg);
	var serverKey2 = serverKey || conf.gcm_server_key;
	var sender = new gcm.Sender(serverKey2);
	sender.send(gcmMsg, regIds, function(err, result){
		if(err) {
			wlog.error({err:err}, {key:key, msg:msg, regIds:regIds, server_key: serverKey2, result:result}, logHead+' ERROR : send GCM');
		}else{
			wlog.error({err:err}, {key:key, msg:msg, regIds:regIds, server_key: serverKey2, result:result}, logHead+' SUCCESS : send GCM');
		}
		cb(err, result);
	});
}

/*
callback signature: cb(err)
 */
exports.sendGcmForChatMsg = function(senderId, partnerId, msgId, cb){
	var logHead = s.getSessionId()+' [control.gcmCtrl.sendGcmForChatMsg]';
	db_users.getUserInfo(senderId, function(err, userInfo){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : get user info');
			return cb(err);
		}
		db_gcmRegId.getRegIds(partnerId, function(err, regIdsFromDb){
			var key = 'data';
			var msg = JSON.stringify({
				type: def.GCM_TYPE.CHAT_MSG,
				partner_id: senderId,
				msg_id: msgId,
				name: userInfo.name,
				profile_pic: conf.domain+userInfo.pic_folder+'/'+userInfo.pic_file
			});
			var regIds = [];
			async.eachSeries(regIdsFromDb, function(regid, cb){
				regIds.push(regid.regid);
				cb(null);
			}, function(err){
				exports.sendGcm(regIds, null, key, msg, function(err, result){
					if(err){
						wlog.error({err:err}, {result:result}, logHead+' ERROR : send gcm');
					}
					cb(err);
				});
			});
		});
	});
}

/*
callback signature: cb(err)
 */
exports.sendGcmForForumSubjectCreation = function(subjectId, cb){
	var logHead = s.getSessionId()+' [control.gcmCtrl.sendGcmForForumSubjectCreation]';
	db_forum.getInfoForForumNoti(
		subjectId,
		function(err, forumId, counselorId, counselorName, clientId){
			db_gcmRegId.getRegIds(counselorId, function(err, regIdsFromDB){
				var key = 'data';
				var msg = JSON.stringify({
					type: def.GCM_TYPE.FORUM_SUBJECT_CREATION,
					forum_id: forumId,
					subject_id: subjectId,
					counselor_id: counselorId,
					counsel_name: counselorName
				});
				var regIds = [];
				async.eachSeries(regIdsFromDB, function(regid, cb){
					regIds.push(regid.regid);
					cb(null);
				}, function(err){
					exports.sendGcm(regIds, null, key, msg, function(err, result){
						if(err){
							wlog.error({err:err}, {result:result}, logHead+' ERROR : send gcm');
						}
						cb(err);
					});
				});
			});
		}
	);
}

/*
 callback signature: cb(err)
 */
exports.sendGcmForForumComment = function(writerId, subjectId, commentId, cb){
	var logHead = s.getSessionId()+' [control.gcmCtrl.sendGcmForForumComment]';
	db_forum.getInfoForForumNoti(
		subjectId,
		function(err, forumId, counselorId, counselorName, clientId){
			var destUserId = null;
			if( writerId === counselorId ) {
				destUserId = clientId;
			}else if( writerId === clientId ) {
				destUserId = counselorId;
			}else{
				err = new Error('writerId(='+writerId+') is neither counselorId(='+counselorId+') nor client(='+clientId+')');
				wlog.error({err:err}, ' ERROR');
				cb(err);
				return;
			}
			db_gcmRegId.getRegIds(destUserId, function(err, regIdsFromDB){
				var key = 'data';
				var msg = JSON.stringify({
					type: def.GCM_TYPE.FORUM_COMMENT,
					forum_id: forumId,
					counselor_id: counselorId,
					counsel_name: counselorName,
					subject_id: subjectId,
					comment_id: commentId
				});
				var regIds = [];
				async.eachSeries(regIdsFromDB, function(regid, cb){
					regIds.push(regid.regid);
					cb(null);
				}, function(err){
					exports.sendGcm(regIds, null, key, msg, function(err, result){
						if(err){
							wlog.error({err:err}, {result:result}, logHead+' ERROR : send gcm');
						}
						cb(err);
					});
				});
			});
		}
	);
}

exports.sendGcmTest = function(req, res, next){
	var logHead = s.getSessionId()+' [control.gcmCtrl.sendGcm]';
	var regIds = [req.body.regid];
	var key = req.body.key || 'TEST KEY';
	var msg = req.body.msg || 'TEST MSG';
	var serverKey = req.body.server_key || null;
	exports.sendGcm(regIds, serverKey, key, msg, function(err, result){
		res.json({
			err:err,
			result:result,
			regid: req.body.regid,
			server_key: serverKey,
			conf_gcm_server_key: conf.gcm_server_key,
			key: key,
			msg: msg
		});
	});
}