/**
 * Created by Shin Yeonsik on 2015-05-06.
 */

var wlog = require('../common/wlogger');
var db_users = require('../model/db_users');
var errJson = require('../common/errJson');
var sessHelp = require('../control/sessionHelper');
var def = require('../common/def');
var facebook = require('../common/facebook');
var wcrypto = require('../common/wcrypto');
var common = require('../common/common');
var async = require('async');
var conf = require('../common/conf');
var db_gcmRegId = require('../model/db_gcmRegId');
var validator = require('validator');
var fs = require('fs');
var c = common;


/*
callback signatrue: cb(err)
 */
var addRegIdForLogin = function(session, regId, loginUserId, cb){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.addRegIdForLogin]';
	db_gcmRegId.addGcmRegId(regId, loginUserId, function(err){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : add gcm regid');
			cb(err);
			return;
		}
		sessHelp.setGcmRegId(session, regId, function(err){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : set regid to session');
			}
			cb(err);
		});
	});
}

/*
callback cb param
	err
	resJson - JSON for response
 */
exports.createTempUser = function(cb){
	var resJson = {};
	db_users.createTempUser(function(err, temp_user_id){
		if(err){
			wlog.debug('[control.userCtrl.createTempUser] ERROR createTempUser ',{err:err});
			resJson = {success:false, error:errJson.ERR_GENERNAL};
		}else{
			resJson = {
				success: true,
				data:{user_id:temp_user_id}
			};
		}
		cb(err, resJson);
	});
}

/*
create user using email and password
	userInfo  - should contain email, password, nickname, gender, birthday, tempUserId(optional)
callback cb param
	err
 */
exports.createUser = function(userInfo, cb){
	db_users.createUser(userInfo, function(err, userId){
		if(err){
			wlog.warn('[control.userCtrl.createUser] Error : ',{err:err});
			return cb(err, null);
		}
		return cb(err, userId);
	});
}

/*
cb param
	err,
	resJson
 */
exports.createUserFacebook = function(userInfo, cb){
	var errGeneralResponse = {success:false, error:errJson.ERR_GENERNAL};
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.createUserFacebook]';
	var resJson = null;
	facebook.getFacebookId(userInfo.access_token, function(err, facebookId){
		if(err){
			wlog.error(logHead+' ERROR : ',{err:err});
			cb(err, errGeneralResponse);
		}else if(null == facebookId){
			err = new Error('facebookId is null');
			wlog.error(logHead+' ERROR : ',{err:err});
			cb(err, errGeneralResponse);
		}else{
			db_users.existFacebookId(facebookId, function(err, exist){
				if(err){
					wlog.error(logHead+' ERROR :',{err:err});
					cb(err, errGeneralResponse);
				}else{
					if(exist){
						wlog.error(logHead+' Already Existing Facebook ID : id='+facebookId);
						cb(err, errGeneralResponse);
					}else{
						userInfo.facebook_id = facebookId;
						db_users.createUserFacebook(userInfo, function(err, success, userId){
							if(err){
								wlog.warn(logHead+' ERROR : ',{err:err});
								cb(err, errGeneralResponse);
								return;
							}else{
								// success json
								resJson = {
									success: true,
									data:{user_id:userId}
								};
							}
							cb(err, resJson);
						});
					}
				}
			});
		}
	});
}

exports.getFavoriteList = function(req, res, next){
	var logPart = 'control.userCtrl.getFavoriteList';
	var logHead = sessHelp.getSessionId()+' ['+logPart+']';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if( !loginUserId ){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}else{
			db_users.getFavoriteList(loginUserId, function(err, result) {
				var favList = [];
				async.each(result, function iter(item, cb) {
					favList.push({
						counselor_id: item.counselor_id,
						profile_pic: conf.domain + item.pic_folder + '/' + item.pic_file,
						name: item.name,
						age_group: common.getAgeGroup(item.birthday),
						gender: item.gender,
						self_intro: item.self_intro
					});
					cb(null);
				}, function (err) {
					if (err) {
						wlog.error({err: err}, logHead + ' ERROR : get favorite list');
						res.json({success: false, error: errJson.ERR_GENERNAL});
						return;
					}else{
						res.json({
							success: true,
							data: {
								size: favList.length,
								counselors: favList
							}
						});
						return;
					}
				});
			});
		}
	});
}

/*
callback signature: cb(err, userInfo)
 */
exports.getLoginUserInfo = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.getLoginUserInfo]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			wlog.error({loginUserId:loginUserId}, logHead+' ERROR : loginUserId Check!');
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		db_users.getUserInfo(loginUserId, function(err, userInfo){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : get login user info');
				res.json({success:false, error:errJson.ERR_INVALID_BIRTHDAY});
				return;
			}
			res.json({
				success:true,
				data:{
					user_id: userInfo.user_id,
					email: userInfo.email,
					name: userInfo.name,
					user_type: userInfo.user_type,
					acc_conn_type: userInfo.acc_conn_type,
					birthday: common.transTime(userInfo.birthday),
					gender: userInfo.gender,
					profile_pic: conf.domain+userInfo.pic_folder+'/'+userInfo.pic_file,
					self_intro: userInfo.self_intro,
					career: userInfo.career,
					location: userInfo.location,
					institute_id: userInfo.institute_id,
					counsel_method: userInfo.counsel_method,
					counsel_target: userInfo.counsel_target,
					edu_background: userInfo.edu_background,
					is_counselor: (userInfo.counsel_type ? true : false)
				}
			});
			return;
		});
	});
}

/*
login
callback cb param
 */
exports.login = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.login]';
	var jsonWrongParam = errJson.ERR_WRONG_PARAM;
	// validate params
	var tempUserId = null;

	if( typeof req.body.type === 'undefined' ){
		res.json(jsonWrongParam);
		return;
	}
	if( typeof req.body.temp_user_id !== 'undefined' ){
		tempUserId = req.body.temp_user_id;
	}
	var regid = req.body.regid || "";
	wlog.debug(logHead+' regid='+req.body.regid);//debug
	if( 0 === regid.length ){
		wlog.error(logHead+'ERROR : invalid regid / regid='+regid);
		res.json({success:false, error:errJson.ERR_INVALID_REGID});
		return;
	}

	var cb_addRegIdForLogin = function(err){
		if(err){
			wlog.error({err:err}, logHead+' ERROR : add regid for login');
		}
	}

	if( def.LOGIN_TYPE.EMAIL_PASSWORD == req.body.type ){
		loginEmailPassword(
			req.session,
			req.body.email,
			req.body.password,
			regid, function(err, success, data) {
				if (err) {
					wlog.error({err: err}, logHead + ' ERROR : email=' + req.body.email);
					res.json({success: false, error:errJson.ERR_GENERNAL});
					return;
				} else if (success) {
					res.json({
						success: true,
						data: {
							user_id: data.userId,
							name: data.name,
							is_counselor: data.isCounselor,
							profile_pic: data.profile_pic
						}
					});
					addRegIdForLogin(req.session, req.body.regid, data.userId, cb_addRegIdForLogin);
					return;
				}else{
					res.json({success:false, error: errJson.ERR_GENERNAL});
					return;
				}
			}
		);
	}else if( def.LOGIN_TYPE.FACEBOOK == req.body.type ){
		loginFacebook(req.session, req.body.access_token, regid, function(err, success, data){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : facebook login fail');
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			if( true === success ){
				res.json({
					success: true,
					data:{
						user_id: data.userId,
						name: data.name,
						is_couselor: data.isCounselor,
						profile_pic: data.profile_pic
					}
				});
				addRegIdForLogin(req.session, req.body.regid, data.userId, cb_addRegIdForLogin);
				return;
			}else{
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
		})
	}
}

/*
cb param
	err, success,
	data = {userId, name, isCounselor}
 */
var loginEmailPassword = function(session, email, password, gcmRegid, cb){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.loginEmailPassword]';
	success = true;
	db_users.getEmailLoginInfo(email, function(err, success, data){
		if(err){
			wlog.error({err:err}, logHead+' FAIL to get password has : email='+email);
			cb(err, false, null);
			return;
		}
		var hashFromInputPassword = wcrypto.hashPassword(password, data.salt);
		if( hashFromInputPassword === data.passwordHash ){
			sessHelp.login(session, data.userId, data.isCounselor, gcmRegid, function(err){
				cb(err, true, {
					userId: data.userId,
					name: data.name,
					isCounselor: data.isCounselor,
					profile_pic: conf.domain+data.pic_folder+'/'+data.pic_file
				});
			});
		}else{
			cb(err, false, null);
		}
	});
}

/*
login with facebook id
callback cb param
	err,
	success,
	data = {userId, name, isCounselor}
	}
 */
var loginFacebook = function(session, accessToken, gcmRegid, cb){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.loginFacebook]';
	facebook.getFacebookId(accessToken, function(err, facebookId){
		if(err){
			wlog.error(logHead+' ERROR : ',{err:err});
			cb(err, false, null);
			return;
		}else{
			db_users.getFacebookLoginInfo(facebookId, function(err, data){
				if(err){
					wlog.error(logHead+' ERROR : ',{err:err});
					cb(err, false, null);
					return;
				}else if(data.userId){
					sessHelp.login(session, data.userId, data.isCounselor, gcmRegid, function(err){
						cb(err, true, data);
						return;
					});
				}else{
					cb(err, false, data);
					return;
				}
			});
		}
	});
}

exports.register = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.register]';
	var invalidParamJson = {success:false, error:errJson.ERR_WRONG_PARAM};

	// validate
	var birthday = req.body.birthday || null;
	if( !validator.isDate(birthday) ){
		wlog.error(logHead+' INVALID birthday='+birthday);
		res.json({success:false, error:errJson.ERR_WRONG_PARAM});
		return;
	}

	if(typeof req.body.type === 'undefined'){
		wlog.error('[/api/user (POST)] WRONG PARAM [type is undefined]');
		res.json(invalidParamJson);
		return;
	}else if(def.ACC_TYPE.NORMAL == req.body.type){
		if( typeof req.body.acc_conn_type === 'undefined' ){
			wlog.error('[/api/user (POST)] WRONG PARAM [acc_conn_type is undefined]');
			res.json(invalidParamJson);
			return;
		}

		var defaultProfilePicFile = null;
		if( def.DB.GENDER_FEMALE === req.body.gender ){
			defaultProfilePicFile = def.FILE.PROFILE_DEFAULT_F;
		}else{
			defaultProfilePicFile = def.FILE.PROFILE_DEFAULT_M
		}

		// normal user, register with email and password
		if( def.ACC_CONN_TYPE.NONE == req.body.acc_conn_type ){
			exports.createUser({
				email: req.body.email,
				password: req.body.password,
				nickname: req.body.nickname,
				birthday: req.body.birthday,
				gender: req.body.gender,
				pic_folder: def.FILE.PROFILE_DEFAULT_PATH,
				pic_file: defaultProfilePicFile,
				temp_user_id: (req.body.temp_user_id || null)
			}, function(err, userId){
				if(err){
					res.json({success:false, error:errJson.ERR_GENERNAL});
					return;
				}
				res.json({success:true, data:{user_id:userId}});
				return;
			});
		}else if( def.ACC_CONN_TYPE.FACEBOOK == req.body.acc_conn_type ){
			// facebook connected user
			exports.createUserFacebook({
				access_token: req.body.access_token,
				nickname: req.body.nickname,
				gender: req.body.gender,
				birthday: req.body.birthday,
				pic_folder: def.FILE.PROFILE_DEFAULT_PATH,
				pic_file: defaultProfilePicFile,
				temp_user_id: (req.body.temp_user_id || null)
			}, function(err, resJson2){
				res.json(resJson2);
				return;
			});
		}
	}else if(def.ACC_TYPE.TEMP == req.body.type){ // temp user id
		exports.createTempUser(function(err, resJson3){
			if(err){
				wlog.error({err:err}, '[/api/user (POST)] ERROR');
			}else{
				wlog.debug({resJson:resJson3},
					'[/api/user (POST)] SUCCESS : create temp user ');
			}
			res.json(resJson3);
			return;
		});
	}else{
		res.json({success:false, error:errJson.ERR_WRONG_PARAM});
		return;
	}
}

/*

 */
exports.setFavorite = function(req, res, next, flag){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.setFavorite]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(loginUserId){
			db_users.setFavorite(
				loginUserId,
				parseInt(req.params.counselor_id),
				flag,
				function(err){
					if(err){
						wlog.error({err:err}, logHead+' ERROR: flag='+flag);
						res.json({success:false, error:errJson.ERR_GENERNAL});
						return;
					}
					res.json({success:true});
			});
		}else{
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
		}
	});
}

exports.update = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.update]';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			c.resjson(req,res,{success:false, error:errJson.ERR_NEED_LOGIN},true);
			return;
		}
		db_users.getUpdateInfo(loginUserId, function(err, oldPwHashFromDB, salt, nickname, acc_conn_type){
			if(err){
				wlog.error({err:err}, ' ERROR');
				c.resjson(req,res,{success:false, error:errJson.ERR_GENERNAL},true);
				return;
			}
			nickname = req.body.nickname || nickname;
			if( 0 == acc_conn_type ){
				var oldPwHash = wcrypto.hashPassword(req.body.old_password, salt);
				if( oldPwHash === oldPwHashFromDB ){
					var newPassword = req.body.new_password || "";
					if(0 === newPassword.length ) newPassword = req.body.old_password;
					db_users.updateNormalAccount(loginUserId, newPassword, nickname, function(err, success){
						if(err){
							wlog.error({err:err}, logHead+' ERROR');
							c.resjson(req,res,{success:false, error:errJson.ERR_GENERNAL},true);
							return;
						}
						if(success){
							c.resjson(req,res,{success:true},true);
						}else{
							c.resjson(req,res,{success:false, error:errJson.ERR_GENERNAL},true);
						}
						return;
					});
				}else{
					c.resjson(req,res,{success:false, error:errJson.ERR_WRONG_PASSWORD},true);
					return;
				}
			}else if( 1 == acc_conn_type ){
				db_users.updateFacebookConnAccount(loginUserId, nickname, function(err, success){
					if(err){
						wlog.error({err:err}, logHead+' ERROR');
						c.resjson(req,res,{success:false, error:errJson.ERR_GENERNAL},true);
						return;
					}
					if( success ){
						c.resjson(req,res,{success:true},true);
					}else{
						c.resjson(req,res,{success:false, error:errJson.ERR_GENERNAL},true);
					}
					return;
				});
			}else{
				wlog.error(logHead+' ERROR : unknown acc_conn_type='+acc_conn_type);
				c.resjson(req,res,{success:false, error:errJson.ERR_GENERNAL},true);
				return;
			}
		});
	});
}

exports.updateProfilePic = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.userCtrl.updateProfilePic]';
	var file = req.files.profile_pic;
	var oldPath = file.path;
	var publicPath = './public';
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		if(!loginUserId){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		var picFolder = '/images/users/'+loginUserId;
		var picFile = 'profile_'+loginUserId+'.'+file.extension;
		var newPath = './public'+picFolder+'/'+picFile;
		fs.mkdir('./public'+picFolder, function(e){
			if(!e || (e && 'EEXIST' === e.code)){
				fs.rename(oldPath, newPath, function(err){
					if(err){
						wlog.error({err:err}, logHead+' ERROR : rename');
						res.json({success:false, error:errJson.ERR_RENAME});
						return;
					}
					db_users.updateProfilePicPath(loginUserId, picFolder, picFile, function(err){
						if(err){
							wlog.error({err:err}, logHead+' ERROR : update db');
							res.json({success:false, error:errJson.ERR_DB_QUERY});
							return;
						}
						res.json({success:true});
						return;
					});
				});
			}else{
				wlog.error({err:e},
					logHead+' ERROR : mkdir [oldPath='+oldPath+' / newPath='+newPath+']');
				res.json({success:false, error:errJson.ERR_MKDIR});
				return;
			}
		});
	});
}