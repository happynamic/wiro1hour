/**
 * Created by Shin Yeonsik on 2015-05-12.
 */

var db_counsel = require('../model/db_counsel');
var sessHelp = require('../control/sessionHelper');
var conf = require('../common/conf');
var errJson = require('../common/errJson');
var async = require('async');
var common = require('../common/common');
var wlog = require('../common/wlogger');


exports.automatch = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.counselCtrl.automatch]';
	db_counsel.automatch(function(err, counselor){
		if(err){
			wlog.error({err:err}, logHead+' ERROR');
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}
		res.json({
			success:true,
			data:{
				counselor_id: counselor.counselor_id,
				name: counselor.name,
				profile_pic: conf.domain+counselor.pic_folder+'/'+counselor.pic_file,
				self_intro: counselor.self_intro
			}
		});
	});
}

exports.list = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.counselCtrl.list]';
	var data = [];
	sessHelp.getLoginUserId(req.session, function(loginUserId){
		db_counsel.getList(loginUserId, function(err, list){
			async.each(list, function(counsel, eachCB){
				var item = {
					counselor_id: counsel.counselor_id,
					profile_pic: conf.domain + counsel.pic_folder+'/'+counsel.pic_file,
					name: counsel.name,
					age_group: common.getAgeGroup(counsel.birthday),
					gender: counsel.gender,
					self_intro: counsel.self_intro,
					favorite: counsel.favorite
				};
				data.push(item);
				eachCB(null);
			}, function eachCB(err){
				if(err){
					wlog.error({err:err}, logHead+' ERROR : build response json');
					resJson = {success: false, error:errJson.ERR_GENERNAL};
				}
				else{
					resJson = {
						success: true,
						data: {
							size: data.length,
							counselors: data
						}
					};
				}
				res.json(resJson);
			});
		});
	});
}

exports.profile = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.counselCtrl.profile]';
	var loginUserId = (req.session.loginUserId || null);
	db_counsel.getProfile(loginUserId, req.params.counselor_id, function(err, profile){
		if(err){
			wlog.error({err:err}, logHead+' ERROR');
			res.json({success:false, error:errJson.ERR_GENERNAL});
			return;
		}
		var resJson = {
			success: true,
			data:{
				counselor_id: profile.counselor_id,
				profile_pic: conf.domain + profile.pic_folder+'/'+profile.pic_file,
				name: profile.name,
				age_group: common.getAgeGroup(profile.birthday),
				gender: profile.gender,
				location: profile.location,
				self_intro: profile.self_intro,
				career: profile.career,
				institute:{
					name: profile.institute_name,
					address: profile.address,
					map: conf.domain + profile.map_pic_folder+'/'+profile.map_pic_filename
				},
				counsel_method: profile.counsel_method,
				counsel_target: profile.counsel_target,
				edu_background: profile.edu_background,
				favorite: profile.favorite
			}
		};
		res.json(resJson);
		return;
	});
}

exports.updateProfile = function(req, res, next){
	var logHead = sessHelp.getSessionId()+' [control.counselCtrl.updateProfile]';
	sessHelp.getLoginUserInfo(req.session, function(loginUserId, isCounselor){
		if(!loginUserId){
			res.json({success:false, error:errJson.ERR_NEED_LOGIN});
			return;
		}
		if(!isCounselor){
			res.json({success:false, error:errJson.ERR_NEED_COUNSEL_LOGIN});
			return;
		}
		var profile = {
			self_intro: req.body.self_intro,
			counsel_method: req.body.counsel_method,
			counsel_target: req.body.counsel_target,
			location: req.body.location,
			career: req.body.career,
			edu_background: req.body.edu_background
		}
		db_counsel.updateProfile(loginUserId, profile, function(err, success){
			if(err){
				wlog.error({err:err}, logHead+' ERROR : update profile + ');
				res.json({success:false, error:errJson.ERR_GENERNAL});
				return;
			}
			res.json({success:true});
			return;
		});
	});
}