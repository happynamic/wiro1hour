var wcrypto = require('../server/common/wcrypto');
var randstring = require('randomstring');
var def = require('../server/common/def');

if( process.argv.length < 3 ){
	console.log("hashGen Usage: node hashGen.js (plain-text) [salt]");
	return;
}

var plain = process.argv[2];
var salt = null;
if( process.argv.length < 4 ){
	salt = randstring.generate(def.SALT_LENGTH);
}else{
	salt = process.argv[3];
}

console.log('plain: '+plain);
console.log('salt: '+salt);

var hash = wcrypto.hashPassword(plain, salt);

console.log('SHA512 Hash: '+hash);